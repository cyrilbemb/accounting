Edit your 'package.json' file and set this part in script node

  "scripts": {
    "ng": "ng",
    "download-eaf-lib": "sh scripts/ci/dl-internal-lib.sh eaf-lib master lastest",
    "up-eaf-lib": "sh scripts/ci/up-3rdParty-lib.sh eaf-lib",
    "remote-up-eaf-lib": "npm run download-eaf-lib && npm run up-eaf-lib",
    "start": "ng serve --open",
    "build": "ng build",
    "prod": "ng build --prod --base-href ./index.html",
    "zip-pack": "cd dist && zip user-management-v2.zip user-management-v2/*",
    "deploy-prod": "sh scripts/ci/deploy.sh",
    "deploy-prod-auto": "npm run prod && npm run zip-pack && npm run deploy-prod",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e"
  },