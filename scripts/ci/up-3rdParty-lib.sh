#!/bin/sh

function npm_update() {
	
	local project=$1;

	local lib_version=`npm list --depth=0 2> /dev/null | grep "+-- $project\@"`;

	if [ x"$lib_version" == x ] ; then
		echo "Try to Install $project";
		npm i ./3rdParty/$project.tgz;
	else
		echo "Try to Update $project";
		npm update $project;
	fi
	local lib_version=`npm list --depth=0 2> /dev/null | grep "+-- $project\@" | cut -c5-`;

	echo "name:    $lib_version" >> "./3rdParty/$project.txt";

	echo "Summary :";
	cat "./3rdParty/$project.txt";
}

npm_update $1
