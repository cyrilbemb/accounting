#!/usr/bin/env bash

##
# Step 5: Tag the repository
##
echo "Tagging for release ${new_version}" "${new_version}"
git tag -a "${new_version}" ${BITBUCKET_COMMIT} -m "Tagging for release ${new_version}"
# git tag "${new_version}"
git push origin ${new_version}
