import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter } from '@angular/material';
import { DataDispatchService } from '../services/data-dispatch.service';
export const DATE_FORMAT = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD MMM YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'DD-MM-YYYY',
  },
};

@Component({
  selector: 'app-accouting-tables',
  templateUrl: './accouting-tables.component.html',
  styleUrls: ['./accouting-tables.component.css']
})


export class AccoutingTablesComponent implements OnInit {
  table_accounting_period = 'accounting_period';
  table_accounting_parameter = 'accounting_parameter';
  table_nationality = 'nationality';
  table_journal_type = 'journal_type';
  table_account_type = 'account_type';
  table_account = 'account';
  table_journal = 'journal';
  table_tax = 'tax';
  table_vat = 'vat';
  table_journal_record = 'journal_record';



  constructor(public dataService : DataDispatchService,public translate : TranslateService,private _adapter: DateAdapter<any>) {

    this._adapter.setLocale(this.dataService.selectedLang);
    this.translate.onLangChange.subscribe(lang =>{
      this._adapter.setLocale(lang.lang);
      console.log(lang);
    });
   }

  ngOnInit() {
  }

}
