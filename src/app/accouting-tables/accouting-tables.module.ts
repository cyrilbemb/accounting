import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccoutingTablesComponent, DATE_FORMAT } from './accouting-tables.component';
import { AppModule } from '../app.module';
import { TranslateModule } from '@ngx-translate/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { SimpleDialogComponent } from '../dialogs/simple-dialog/simple-dialog.component';
import { InputDialogComponent } from '../dialogs/input-dialog/input-dialog.component';
import { DbTableFormComponent } from '../dialogs/db-table-form/db-table-form.component';
import { TableDetailsComponent } from '../table-details/table-details.component';
import { MaterialFeaturesModule } from '../material-features/material-features.module';
import { TableJournalTypeComponent } from '../tables-specials/table-journal-type/table-journal-type.component';
import { TableAccountTypeComponent } from '../tables-specials/table-account-type/table-account-type.component';
import { TableTaxComponent } from '../tables-specials/table-tax/table-tax.component';
import { TableVatComponent } from '../tables-specials/table-vat/table-vat.component';
import { TableJournalEntryComponent } from '../tables-specials/table-journal-entry/table-journal-entry.component';
import { TableEntryComponent } from '../tables-specials/table-journal-entry/table-entry/table-entry.component';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { ImportDataComponent } from '../import-data/import-data.component';

@NgModule({
  declarations: [
    AccoutingTablesComponent,
    TableDetailsComponent,
    SimpleDialogComponent,
    InputDialogComponent,
    DbTableFormComponent,
    TableJournalTypeComponent,
    TableAccountTypeComponent,
    TableTaxComponent,
    TableVatComponent,
    TableJournalEntryComponent,
    TableEntryComponent,
    ImportDataComponent
  ],
  imports: [
    MaterialFeaturesModule,
    CommonModule,
    TranslateModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-EN'},
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    {provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT},
  ],
  exports:[
    AccoutingTablesComponent
  ],
  entryComponents:
  [InputDialogComponent,
    SimpleDialogComponent,
    DbTableFormComponent
  ],
})

export class AccoutingTablesModule {

}
