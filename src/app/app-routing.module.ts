import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'eaf-lib';
import { AccoutingTablesModule } from './accouting-tables/accouting-tables.module';
import { AccoutingTablesComponent } from './accouting-tables/accouting-tables.component';
import { TableJournalEntryComponent } from './tables-specials/table-journal-entry/table-journal-entry.component';
import { ImportDataComponent } from './import-data/import-data.component';

const routes: Routes = [

  {
    path: 'tables',
    component: AccoutingTablesComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'importdata',
    component: ImportDataComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'journalrecord',
    component: TableJournalEntryComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: '**',
    redirectTo: 'tables',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]

})
export class AppRoutingModule { }
