import { Component } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { MatIconRegistry, MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material';
import { RouterInitializerService, UserDfApiService, AmUser, RouterInitializerEvent  } from 'eaf-lib';
import { FlatTreeControl } from '@angular/cdk/tree';
import { DataDispatchService } from './services/data-dispatch.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  private _transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      link: node.link
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);



  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
  title = 'Accounting';

  me: AmUser;

  constructor(public dataService : DataDispatchService,public translate: TranslateService,public matIconRegistry: MatIconRegistry,
    private routerInitializerService: RouterInitializerService, private bootApp: RouterInitializerService, public userService : UserDfApiService
  ) {
    this.dataSource.data = TREE_DATA;
    matIconRegistry.registerFontClassAlias('fa');
    this.bootApp.fetchAmUserOnSessionChanged = false;

    this.bootApp.userChanging$.subscribe(
      (x) => this.userChanging(x)
    );



    this.bootApp.initializeRouter();
    // Translation
    // ****************************************************************/
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    // ****************************************************************/
    this.dataService.selectedLang = browserLang.match(/en|fr/) ? browserLang : 'en';
    translate.addLangs(['en', 'fr']);

    /*this.sharedService.ormMerge({
      orm_name: "orm_accounting",
      table_name: "journal_type",
      key_field_source_value :"ENG",
      merge_field_name: "journal_type_id",
      key_field_name: "lang",
      key_field_destination_value : 'FRA'
    } ).subscribe(result =>{
      console.log(result);
    });*/

    this.translate.onLangChange
    .subscribe((event: LangChangeEvent) => {
      // REMOVE this line to make it work
      // this.translate.use(event.lang);
      this.dataService.selectedLang = event.lang;
      console.log('lang change',event);
  });
  }


  routerLinktst(node: any){
      console.log(node);
  }

  logout() {
    this.bootApp.logout('login');
  }
  userChanging(ev: RouterInitializerEvent) {
    // console.log('userChanging', ev);

    if (!ev.session) {
      this.bootApp.locateLiteToRedirectURL(ev.moveTo, 'login');
    }else{

    }

    this.bootApp.navigate(ev.moveTo);
  }
}

interface FoodNode {
  name: string;
  children?: FoodNode[];
  link?: string;
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'importdata',
    link: 'importdata'
  },{
    name: 'tables',
    link: 'tables'
  }, {
    name: 'journal_record',
    /*children: [
      {
        name: 'Green',
        children: [
          {name: 'Broccoli', link: 'login'},
          {name: 'Brussels sprouts', link: 'login'},
        ], link: 'login'
      }, {
        name: 'Orange',
        children: [
          {name: 'Pumpkins', link: 'login'},
          {name: 'Carrots', link: 'login'},
        ], link: 'login'
      },
    ],*/ link: 'journalrecord'
  },
];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  link: string;
}
export class TreeFlatOverviewExample {

}
