import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';



import { DfApiModule, Configuration, LoginModule } from 'eaf-lib';
import { MaterialFeaturesModule } from './material-features/material-features.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { AccoutingTablesModule } from './accouting-tables/accouting-tables.module';
import { ImportDataComponent } from './import-data/import-data.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  // if translation file are stored on another file server
  // return new TranslateHttpLoader(http, 'http://mytranslations.com/i18n/');
}

export function dfApiConfigurationFactory() {

    return new Configuration(environment.dfApi);

}

@NgModule({
  declarations: [
    AppComponent,
   ],
  imports: [

    BrowserModule,
    HttpClientModule,
    MaterialFeaturesModule,
    AccoutingTablesModule,
    DfApiModule.forRoot(dfApiConfigurationFactory),
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    LoginModule,
    BrowserModule,
    AppRoutingModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
