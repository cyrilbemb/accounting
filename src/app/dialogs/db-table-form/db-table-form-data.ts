export interface DbTableFormData {

    tableName: string;
    tableRecord: any;
    siteId : string;

    translationClassName: string;

}
