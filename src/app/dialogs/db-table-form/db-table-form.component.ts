import { Component, OnInit, Inject, ɵConsole } from '@angular/core';
import { DbTableFormData } from './db-table-form-data';
import { TranslateService } from '@ngx-translate/core';
import { InputDialogComponent } from '../../dialogs/input-dialog/input-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { DbTableService } from 'src/app/services/db-table-service/db-table.service';
import { ModelTableObject } from 'src/app/models/model-table-object';
import { OrmAccountingService } from 'src/app/services/db-table-service/orm-accounting.service';
import { ModelSettings } from 'src/app/models/model-settings';
import { DataTableInfo } from 'src/app/models/model-table-info';
import { DataDispatchService } from 'src/app/services/data-dispatch.service';
import { OrmAmdirectoryService } from 'src/app/services/db-table-service/orm-amdirectory.service';

@Component({
  selector: 'app-db-table-form',
  templateUrl: './db-table-form.component.html',
  styleUrls: ['./db-table-form.component.css']
})
export class DbTableFormComponent implements OnInit {
  protected modelSettings = ModelSettings;

  protected tableSchema:DataTableInfo;

  public formFields = new Map([]);
  public formRecord: any;
  public formRecordTst: any;

  public linked_data : string[];
  protected userSite : string = null;

  protected tableFormData: DbTableFormData;

  protected referenceTables = new Map([]);
  protected foreignKeys = new Map([]);

  formatDateToLocaleFunction = function formatDateToLocale(dbUtcDate : Date): string {

    if(dbUtcDate == undefined)
        return '[Invalid Date]';

    //Not working on Firefox and IE11
    //**********************************************/
    //let localeDate = new Date(utcDate + ' UTC');
    //return localeDate.toLocaleString();
    //**********************************************/

    let dateLocaleStr:string = '';
    try {

        let utcStringDate = dbUtcDate.toString();
        utcStringDate = utcStringDate.replace(/ /g,'T') + '+00:00';

        let utcDateObj =  new Date(utcStringDate);
        let parsedDate = Date.parse(utcDateObj.toString());

        if(isNaN(parsedDate)) {
            return dbUtcDate.toString();
          } else {
            dateLocaleStr = utcDateObj.toLocaleString();
          }

    } catch (error) {
        console.log("[error] formatDateToLocale");
        return dbUtcDate.toString();
    }

    return dateLocaleStr;
 };

  constructor(
    public translate: TranslateService,
    protected ormAmdirectoryService: OrmAmdirectoryService,
    protected ormAccountingService: OrmAccountingService,
    public dialogRef: MatDialogRef<InputDialogComponent>,
    public dataService: DbTableService,
    public dataDispatch : DataDispatchService,
    public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public formData: DbTableFormData) {

      this.tableFormData = formData;
      this.tableSchema = ModelTableObject.getTableModel(formData.tableName, ormAmdirectoryService,ormAccountingService,dataDispatch, translate);

      this.userSite = formData.siteId;
      console.log("formData", formData);
      console.log(" this.userSite",  this.userSite);
      this.formFields = this.tableSchema.field_access;

      this.formRecord = formData.tableRecord;

      this.linked_data = this.tableSchema.linked_data;
    }

  ngOnInit() {


    console.log('BASE SCHEMA',this.tableSchema);

    this.tableSchema.field_reference_table.forEach((value: string, key: string) => {

      console.log('NGONINIT',key, value);

      let foreignTable = ModelTableObject.getTableModel(value,this.ormAmdirectoryService, this.ormAccountingService,this.dataDispatch, this.translate);
      foreignTable.getTableRecords(this.userSite);

      this.referenceTables[key] = foreignTable;
      console.log('FOREIGN TABLE',this.referenceTables);
    });

    this.foreignKeys = this.tableSchema.field_foreign_key;
  }

  isFieldRequired(fieldName: string) : boolean {
    return this.tableSchema.mandatory_fields.includes(fieldName);
  }

  isFieldDisplayed(field) : boolean {
    let fieldVisible = true;

    if(field.value == ModelSettings.FIELD_HIDDEN) {
      fieldVisible = false;
    }
    else {
      if(this.formRecord!= undefined && this.tableSchema.non_editable_fields.indexOf(field.key) != -1
        && (this.formRecord[field.key] == undefined || this.formRecord[field.key] == '')) {
        fieldVisible = false;
      }
    }

    return fieldVisible;
  }

  formatFieldValue(fieldValue:any): string {

    let parsedDate = Date.parse(fieldValue);

    if(isNaN(parsedDate)) {
      return fieldValue;
    } else {
      return this.formatDateToLocaleFunction(fieldValue);
    }

  }

  getFieldChoices(tableFieldName: string) {
    //let refTable =

  }

  dateChanged(value : any){
    let date = new Date(value);

    date.setUTCHours(date.getUTCHours()+12);
    console.log( date);
    this.formRecord[value] = date;
    console.log( this.formRecord[value]);
  }

  getItemText(foreignTable:DataTableInfo,  item) {

    let itemText = item[foreignTable.main_displayed_field];

    /*foreignTable.additional_displayed_fields.forEach( field => {
      itemText += " - " + item[field];
    });*/

    return itemText;
  }

  onSelectionChange(event, fieldName) {
    console.log('onSelectionChange',fieldName,event);
    console.log('onSelectionChange',this.formRecord[fieldName]);
    console.log('onSelectionChange',this.formRecord);
    this.formRecord[fieldName] = event.value;
  }

  asIsOrder(a, b) {
    return 1;
  }

  onCancel() {
    this.dialogRef.close();
  }

  consoleLog() {
    console.log(this.formRecord);

    this.formRecordTst = JSON.parse(JSON.stringify(this.formRecord));
  }

  onValidateForm() {
    console.log('VALIDATE',this.formRecord,this.formRecordTst);
    let cancel = false;
     //Check mandatory fields
     let mandatoryFields = [];
     this.formFields.forEach((value: string, key: string) => {
      if (this.isFieldRequired(key)
            && (this.formRecord[key] == '' || this.formRecord[key] == undefined)) {
              mandatoryFields.push(key);
          }
    });


    this.linked_data.forEach(cond => {

      if (cond.split('<DATE<').length>1){
        let fields: string[] = cond.split('<DATE<');

        if(new Date(this.formRecord[fields[0]]) < new Date(this.formRecord[fields[1]])){

        }else{
          console.log(fields[0],this.formRecord[fields[0]],fields[1],this.formRecord[fields[1]])
          cancel = true;
          this.snackBar.open(this.translate.instant(this.tableFormData.tableName+'.'+fields[0])+" "+ this.translate.instant('error_messages.error_field_inferior')+" "+
          this.translate.instant(this.tableFormData.tableName+'.'+fields[1]), this.translate.instant('ok'), {
            duration: 5000,
            panelClass: ['warning-snackbar']
          });
        }
      }
      else if(cond.split('>DATE>').length>1){

        let fields: string[] = cond.split('>DATE>');
        if(new Date(this.formRecord[fields[0]]) > new Date(this.formRecord[fields[1]])){

        }else{
          cancel = true;
          this.snackBar.open(this.translate.instant(this.tableFormData.tableName+'.'+fields[0])+" "+ this.translate.instant('error_messages.error_field_superior')+" "+
          this.translate.instant(this.tableFormData.tableName+'.'+fields[1]), this.translate.instant('ok'), {
            duration: 5000,
            panelClass: ['warning-snackbar']
          });
        }
      }else if (cond.split('<').length>1){
        let fields: string[] = cond.split('<');

        if(this.formRecord[fields[0]] < this.formRecord[fields[1]]){

        }else{
          console.log(fields[0],this.formRecord[fields[0]],fields[1],this.formRecord[fields[1]])
          cancel = true;
          this.snackBar.open(this.translate.instant(this.tableFormData.tableName+'.'+fields[0])+" "+ this.translate.instant('error_messages.error_field_inferior')+" "+
          this.translate.instant(this.tableFormData.tableName+'.'+fields[1]), this.translate.instant('ok'), {
            duration: 5000,
            panelClass: ['warning-snackbar']
          });
        }
      }
      else if(cond.split('>').length>1){

        let fields: string[] = cond.split('>');
        if(this.formRecord[fields[0]] > this.formRecord[fields[1]]){

        }else{
          cancel = true;
          this.snackBar.open(this.translate.instant(this.tableFormData.tableName+'.'+fields[0])+" "+ this.translate.instant('error_messages.error_field_superior')+" "+
          this.translate.instant(this.tableFormData.tableName+'.'+fields[1]), this.translate.instant('ok'), {
            duration: 5000,
            panelClass: ['warning-snackbar']
          });
        }
      }else if(cond.split('===').length>1){
        let fields: string[] = cond.split('===');
        if(this.formRecord[fields[0]] === this.formRecord[fields[1]]){

        }else{
          cancel = true;

          this.snackBar.open(this.translate.instant(this.tableFormData.tableName+'.'+fields[0])+" "+ this.translate.instant('error_messages.error_field_equals')+" "+
          this.translate.instant(this.tableFormData.tableName+'.'+fields[1]), this.translate.instant('ok'), {
            duration: 5000,
            panelClass: ['warning-snackbar']
          });
        }
      }

      else if(cond.split('|REG|').length>1){
        let fields: string[] = cond.split('|REG|');
        let regToCheck = new RegExp(fields[1]);
        if(regToCheck.test(this.formRecord[fields[0]])){

        }else{
          cancel = true;
          this.snackBar.open(this.translate.instant(this.tableFormData.tableName+'.'+fields[0])+" "+ this.translate.instant('error_messages.error_field_reg'), this.translate.instant('ok'), {
            duration: 5000,
            panelClass: ['warning-snackbar']
          });
        }
      }

    });

    if(mandatoryFields.length > 0) {
      this.snackBar.open(this.translate.instant('warning_mandatory_fields'), this.translate.instant('ok'), {
        duration: 5000,
        panelClass: ['warning-snackbar']
      });
    }
    else {
      if(!cancel){
        console.log('Form not Canceled',this.formRecord);
      this.dataService.updateItem(this.formRecord);
      //this.dialogRef.close(this.formRecord);
      this.dialogRef.close(1);
      }
    }


  }

}
