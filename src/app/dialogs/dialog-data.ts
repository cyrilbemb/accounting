export interface DialogData {

    siteId: string;

    title: string;
    label: string;
    message:string;
    value: any;

    // dialog buttons
    hideCancel: boolean;
    yesNoButtons: boolean;

    //for selection list
    referenceTableName: string;
    id_field: string;
    main_field: string;
    additional_fields: string[];
    //items: any[];
}
