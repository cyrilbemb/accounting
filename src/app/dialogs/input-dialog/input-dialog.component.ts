import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DialogData } from '../dialog-data';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-input-dialog',
  templateUrl: './input-dialog.component.html',
  styleUrls: ['./input-dialog.component.css']
})
export class InputDialogComponent implements OnInit {

  private initialValue: any;

  constructor(
    public translate: TranslateService,
    public dialogRef: MatDialogRef<InputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.initialValue = data.value;
    }

    ngOnInit() { }

  onCancel(): void {
    this.dialogRef.close(this.initialValue);
  }

  onEnterKey(event) {
      this.dialogRef.close(this.data.value);
  }


}
