import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DialogData } from '../dialog-data';
import { InputDialogComponent } from '../input-dialog/input-dialog.component';

@Component({
  selector: 'app-simple-dialog',
  templateUrl: './simple-dialog.component.html',
  styleUrls: ['./simple-dialog.component.css']
})
export class SimpleDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<InputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
    //console.log(this.data.message);
  }

  onCancel() {
    this.dialogRef.close(-1);
  }

  onValidate() {
    this.dialogRef.close(1);
  }

}
