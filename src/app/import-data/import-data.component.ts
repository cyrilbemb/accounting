import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag } from '@angular/cdk/drag-drop';
import { DataParserService } from '../services/data-parser.service';
import { IImportHeader } from '../models/import-header';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher, MatDialog, MatSnackBar } from '@angular/material';
import { IJournalEntry } from '../models/journal-entry';
import { ModelTableObject } from '../models/model-table-object';
import { INationality } from '../models/nationality';
import { IAccountingParameters } from '../models/accounting-parameters';
import { IAccountingPeriod } from '../models/accounting-period';
import { IAccount } from '../models/account';
import { IJournal } from '../models/journal';
import { TableSchema, FieldSchema } from 'eaf-lib';
import { OrmAccountingService } from '../services/db-table-service/orm-accounting.service';

import { v4 as uuidv4 } from 'uuid';
import { SimpleDialogComponent } from '../dialogs/simple-dialog/simple-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
import { DbTableFormComponent } from '../dialogs/db-table-form/db-table-form.component';
import * as moment from 'moment';
import { IJournalPiece } from '../models/journal-piece';
import { DbTableService } from '../services/db-table-service/db-table.service';
import { SharedCustomService } from '../services/custom-services/shared-custom.service';
import { ModelSettings } from '../models/model-settings';
import { DataTableInfo } from '../models/model-table-info';
import { OrmAmdirectoryService } from '../services/db-table-service/orm-amdirectory.service';
import { DataDispatchService } from '../services/data-dispatch.service';
//import * as csvParse from 'csv-parse';

@Component({
  selector: 'app-import-data',
  templateUrl: './import-data.component.html',
  styleUrls: ['./import-data.component.css']
})
export class ImportDataComponent implements OnInit {

  recordSeparator = new FormControl();
  recordSerparatorStr : string = '';

  fieldSeparator = new FormControl();
  fieldSeparatorStr : string = '';

  displayArr : any[][] = [];

  tableName : string;

  public TABULATION_VALUE = '\t';
  public CARRIAGE_RETURN_VALUE = '\r';
  public NEW_LINE_VALUE = '\n';
  public PIPE_VALUE = '|';
  public SEMICOLON_VALUE = ';';
  public COMMA_VALUE = ',';

  public fileReaded : File;
  public fileRecordList : any[];

  public FIELD_EDIT = 'F';
  public RECORD_EDIT = 'R';

  public IMPORT_JSON :string = 'json';
  public IMPORT_XML : string = 'xml';
  public IMPORT_CUSTOM : string = 'text';
  public IMPORT_CSV : string = 'csv';
  public IMPORT_TSV : string = 'tsv';

  selectedModel : ModelTableObject = undefined ;
  tableInfo : DataTableInfo = undefined ;
  fieldList : Array<FieldSchema>;

  importHeaders : IImportHeader = undefined;

  improtHeadersList : IImportHeader[] = [];


  tempDocument : IJournalPiece;

  constructor(public sharedService : SharedCustomService,public dataService: DbTableService,public snackBar : MatSnackBar ,public translate: TranslateService,public dialog: MatDialog,private ormAmdirectoryService: OrmAmdirectoryService,
    private ormAccountingService: OrmAccountingService,
    public dataDispatch : DataDispatchService,public dataPaser :  DataParserService) { }

  ngOnInit() {
    this.ormAccountingService.getRecords('impex_header').subscribe(result =>{
      this.improtHeadersList = result.resource;
    });
  }


  drop(event: CdkDragDrop<string[]>) {
    console.log(event);
    let saveObj = this.displayArr[event.previousIndex];
    this.displayArr[event.previousIndex] = this.displayArr[event.currentIndex];
    this.displayArr[event.currentIndex] = saveObj;
    //moveItemInArray(this.displayArr, event.previousIndex, event.currentIndex);
  }

  /** Predicate function that only allows even numbers to be dropped into a list. */
  evenPredicate(item: CdkDrag<number>) {
    return item.data % 2 === 0;
  }

  /** Predicate function that doesn't allow items to be dropped into a list. */
  noReturnPredicate() {
    return false;
  }

  removeHeader(index : number){
    this.fieldList.splice(index,1);
  }

  removeDataList(index : number){
    this.displayArr.splice(index,1);
  }

  newImportHeader(){
    this.importHeaders = new IImportHeader();
  }

  addAttachment(fileInput: any) {
    this.fileReaded = fileInput.target.files[0];
    //  handle the rest
    this.importHeaders.import_source_filename = this.fileReaded.name;
    console.log('file',this.fileReaded);


  }

  csvInputChange(fileInputEvent: any) {
    console.log(fileInputEvent.target.files[0]);
  }

  consoleLog(tst){
    console.log(tst);
  }

  change(vr: string, event)
  {
    console.log(event);
    if(event.isUserInput) {

      if(event.source.selected){
        if(vr === this.RECORD_EDIT){
          this.recordSerparatorStr = this.recordSerparatorStr + event.source.value;
        }else if(vr === this.FIELD_EDIT){
          this.fieldSeparatorStr = this.fieldSeparatorStr + event.source.value;
        }
      }else{
        if(vr === this.RECORD_EDIT){
          this.recordSerparatorStr = this.recordSerparatorStr.replace(event.source.value,'');
        }else if(vr === this.FIELD_EDIT){
          this.fieldSeparatorStr = this.fieldSeparatorStr.replace(event.source.value,'');
        }
      }

      this.importHeaders.import_end_of_line = this.recordSerparatorStr;
      this.importHeaders.import_field_separator = this.fieldSeparatorStr;
    }
  }

  changeTable(event)
  {
    console.log(event);
    if(event.isUserInput) {

      if(event.source.value != undefined){
        this.selectedModel = this.getNewItem(event.source.value);
        this.tableInfo = ModelTableObject.getTableModel('journal_entry',this.ormAmdirectoryService, this.ormAccountingService,this.dataDispatch, this.translate);
        console.log('TABLE INFO',this.tableInfo);

        this.loadHeaders(event.source.value);
      }

    }
  }

  changeType( event)
  {

    if(event.isUserInput) {
      let newType = event.source.value;

      this.importHeaders.import_end_of_line = '';
      this.importHeaders.import_field_separator = '';
      console.log(event);
      switch(newType){
        case this.IMPORT_CSV:
          this.importHeaders.import_end_of_line = this.importHeaders.import_end_of_line.concat(this.CARRIAGE_RETURN_VALUE,this.NEW_LINE_VALUE);
          this.importHeaders.import_field_separator = this.importHeaders.import_field_separator.concat(this.SEMICOLON_VALUE);
          break;
        case this.IMPORT_TSV:
          this.importHeaders.import_end_of_line = this.importHeaders.import_end_of_line.concat(this.CARRIAGE_RETURN_VALUE,this.NEW_LINE_VALUE);
          this.importHeaders.import_field_separator = this.importHeaders.import_field_separator.concat(this.TABULATION_VALUE);
          break;
        default:
          this.importHeaders.import_end_of_line = '';
          this.importHeaders.import_field_separator = '';
          break;
      }

      console.log(this.importHeaders);

    }
  }

  readFile(){
    console.log(this.importHeaders.import_format);
    if(this.importHeaders.import_format != this.IMPORT_JSON && this.importHeaders.import_format != this.IMPORT_XML){
      this.dataPaser.stringToObjectList(this.fileReaded,this.importHeaders.import_field_separator,this.importHeaders.import_end_of_line,this.importHeaders.import_row_preview).subscribe(recordList =>{
        this.fileRecordList = recordList;

        this.displayArr = this.changeRowOrder(this.fileRecordList);
        this.dataPaser.stringToObjectList(this.fileReaded,this.importHeaders.import_field_separator,this.importHeaders.import_end_of_line).subscribe(recordList =>{
          this.fileRecordList = recordList;
        });
      });
    }else if(this.importHeaders.import_format == this.IMPORT_JSON){
      this.dataPaser.jsonToObjectList(this.fileReaded).subscribe(result => {
        this.fileRecordList = result;
        let keys = Object.keys(this.fileRecordList[0]);
        keys.forEach(element => {
          this.displayArr.push([element]);
        });
        console.log('parserJSON',this.displayArr,result,this.fileRecordList);

      });
    }else if(this.importHeaders.import_format == this.IMPORT_XML){
      this.dataPaser.xmlToObjectList(this.fileReaded).subscribe(result => {
        this.fileRecordList = result;
        this.displayArr = this.fileRecordList;
        console.log('parserXML',this.displayArr,result,this.fileRecordList);
      });
    }

  }

  public changeRowOrder(recordList : any [][]) : any[][]{
    let recordListCopy : any[][] = recordList;
    console.log('change order');
    let tempArr : any[][] = [];

    if(recordListCopy.length>0){
      for(let y = 0; y< recordListCopy[0].length ; y++){

        let rowLine : any[] = [];

        for (let x = 0; x < recordListCopy.length; x++) {
          rowLine.push(recordListCopy[x][y]);
        }

        tempArr.push(rowLine);
      }
    }

    return tempArr;

  }

  getNewItem(tablename : string): Object {

    switch (tablename) {

      case ('accounting_parameter'):
       const newParam: IAccountingParameters = {
         organisation_id: 'EAA'
       };
       return newParam;
       break;
     case ('nationality'):
       const newNationality: INationality = {
         organisation_id: 'EAA',
         nationality_id:''
       };
       return newNationality;
       break;
     case ('account'):
       const newAccount: IAccount = {
         organisation_id: 'EAA'
       };
       return newAccount;
       break;
     case ('journal'):
       const newJournal: IJournal = {
         organisation_id: 'EAA'
       };
       return newJournal;
       break;
     case ('journal_piece'):
       let newDate = moment(new Date())
        const newJournalPiece: IJournalPiece = {
          uuid: uuidv4(),
          journal_id : '4',
          accounting_period_id : '1',
          organisation_id : 'EAA',
          site_id : 'EAF',
          journal_entry_date : new Date(),
          journal_entry_due_date : new Date(),
          journal_entry_value_date : new Date(),
          duedate : new Date()


         };
     return newJournalPiece;




   break;
     case ('journal_entry'):
       const newJournalEntry: IJournalEntry = {
        uuid: uuidv4(),
        journal_piece_uid : this.tempDocument.uuid,
        journal_entry_item : this.tempDocument.accounting_period_id
       };
       return newJournalEntry;
       break;
    }
    return null;
   }

   private loadHeaders(tableName : string) {

    this.ormAccountingService.getSchemaDetails(tableName, false, null, false)
      .subscribe(
      (dataRow) => {
        let schema = dataRow as TableSchema;
        this.fieldList = schema.field;

      });

  }

  selectedHeaderChanged(event){
    console.log(event);

  }

  saveHeader(){
    let resource: Array<any> = [];

    console.log(this.importHeaders,this.recordSerparatorStr,this.fieldSeparatorStr);
    this.importHeaders.uuid = uuidv4();
    this.importHeaders.import_end_of_line = this.recordSerparatorStr;
    this.importHeaders.import_field_separator = this.fieldSeparatorStr;
    resource.push(this.importHeaders);
    this.ormAccountingService.postRecord('impex_header',{resource : resource}).subscribe(result => {
      if(result.resource.length >0){
        this.snackBar.open(this.translate.instant('save_succes'), this.translate.instant('ok'), {
          duration: 5000,
          panelClass: ['success-snackbar']
        });
      }
    });
  }

  createPiece(){
    const uldFormDialog = this.dialog.open(DbTableFormComponent, {
      minWidth: '600px',
      maxHeight: '90vh',
      data: {
       tableName: 'journal_piece',
       tableRecord: this.getNewItem('journal_piece'),
       organisation_id: 'EAA',
       translationClassName: 'journal_piece'
      }
    });

    uldFormDialog.afterClosed().subscribe(result => {
      if (result === 1) {
        const editedItem = this.dataService.getDialogData();
        this.tempDocument = editedItem;
      }
    });
  }

  editPiece(){

  }
  startMapping(){

    let map : Map<string,number> = new Map();

    this.sharedService.getCounter('orm_accounting','journal.piece','EAA','EAF').subscribe(result => {
        this.tempDocument.journal_piece_id = result.resource;

        console.log(this.displayArr,this.fileRecordList);
    this.fieldList.forEach(field => {

      if(this.displayArr[this.fieldList.indexOf(field)] != undefined){
        if(this.importHeaders.import_format != this.IMPORT_XML && this.importHeaders.import_format != this.IMPORT_JSON){
          console.log(this.displayArr[this.fieldList.indexOf(field)][0]);
          map.set(field.name,this.fileRecordList[0].indexOf(this.displayArr[this.fieldList.indexOf(field)][0]));
        }else if(this.importHeaders.import_format == this.IMPORT_JSON){
          console.log(this.displayArr[this.fieldList.indexOf(field)][0]);
          map.set(field.name,this.displayArr[this.fieldList.indexOf(field)][0]);
        }

      }

    });
    console.log(map);

    let impList : any[] = [];
    this.fileRecordList.forEach(record => {
      if(this.fileRecordList.indexOf(record) > this.importHeaders.import_row_ignore){
        let impEntry = this.getNewItem('journal_entry');
        for (const [key, value] of map.entries()) {

          if(value != -1){
           // console.log(key,this.tableInfo.field_access[key]);
            if(this.tableInfo.field_access.get(key) == ModelSettings.FIELD_DATE){
              //console.log(key,'DATE');
              impEntry[key] = moment( record[value],'DD/MM/YYYY');
            }else{
              impEntry[key] = record[value];
            }


          }

        }


        impList.push(impEntry);


      }

      });

      console.log(impList);


      this.ormAccountingService.postRecord('journal_piece',{resource : [this.tempDocument]}).subscribe(result =>{
        this.ormAccountingService.postRecord('journal_entry',{resource : impList},true).subscribe(result =>{

        });
      });
    });



  }

  updateHeader(){
    let resource: Array<any> = [];

    resource.push(this.importHeaders);

    this.importHeaders.import_end_of_line = this.recordSerparatorStr;
    this.importHeaders.import_field_separator = this.fieldSeparatorStr;
    this.ormAccountingService.patchRecords('impex_header',{resource : resource}).subscribe(result => {
      if(result.resource.length >0){
        this.snackBar.open(this.translate.instant('update_succes'), this.translate.instant('ok'), {
          duration: 5000,
          panelClass: ['success-snackbar']
        });
      }
    });
  }

  deleteHeader(){

    const formDialog = this.dialog.open(SimpleDialogComponent, {
    minWidth: '300px',
    maxHeight: '100vh',
    data: {
      message:  this.translate.instant('prompt_delete_admin'),
      title: this.importHeaders.import_name
    }
  });

  formDialog.afterClosed().subscribe(result => {
    if (result === 1) {
    let resource: Array<any> = [];

    resource.push(this.importHeaders);

    this.importHeaders.import_end_of_line = this.recordSerparatorStr;
    this.importHeaders.import_field_separator = this.fieldSeparatorStr;
    this.ormAccountingService.deleteRecords('impex_header', null, ['*'], [], [], [], [], null, null, "(uuid = '"+this.importHeaders.uuid+"')").subscribe(result => {
      console.log(result);
      if(result.resource.length >0){

        this.snackBar.open(this.translate.instant('delete_succes'), this.translate.instant('ok'), {
          duration: 5000,
          panelClass: ['success-snackbar']
        });

        this.improtHeadersList.splice(this.improtHeadersList.indexOf(this.improtHeadersList.find(x => x.uuid == this.importHeaders.uuid)),1);
        this.importHeaders = undefined;
      }
    });
  }
  });
  }
}
