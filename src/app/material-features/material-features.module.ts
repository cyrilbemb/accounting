/**
 * Commenter ce qui n'est pas utile dans l'application
 * le Tree Shaking fera le reste
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import {PlatformModule} from '@angular/cdk/platform';

import {MatFormFieldRequiredDirective} from './mat-form-field-required.directive';
;
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
//import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import { MatMomentDateModule, MomentDateAdapter } from "@angular/material-moment-adapter";

import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import {
  MatFormFieldModule,
  // MatAutocompleteModule,
  MatBadgeModule,
  // MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  // MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  // MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  // MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  // MatSliderModule,
  // MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  // MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule
} from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    PlatformModule,
    NgxMatSelectSearchModule,
    FlexLayoutModule,
    MatFormFieldRequiredDirective,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    DragDropModule,
    MatDatepickerModule,
    MatDialogModule,
    MatNativeDateModule,  // or MatMomentDateModule
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatGridListModule,
    MatBadgeModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatMomentDateModule,
    MatTooltipModule,
    MatSelectModule,
    MatListModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatRadioModule,
    MatPaginatorModule,
    MatSortModule,
    MatTreeModule
  ],
  declarations: [
    MatFormFieldRequiredDirective
  ]
})
export class MaterialFeaturesModule { }
