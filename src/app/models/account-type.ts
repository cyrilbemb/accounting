import { DataTableInfo } from './model-table-info';

import { ModelSettings } from './model-settings';
import { OrmAccountingService } from '../services/db-table-service/orm-accounting.service';
import { map, catchError } from 'rxjs/operators';
import { ResultResponse, ResourceResponse } from 'eaf-lib';
import { of } from 'rxjs';
import { DataDispatchService } from '../services/data-dispatch.service';

export class IAccountType {
  id?:    string;
  account_type_id?:   string;
  lang?:          string;

  account_type_name?:          string;

}

export class AccountTypeTable extends DataTableInfo {



  public id_field = 'account_type_id';
  public main_displayed_field = 'account_type_name';
  public additional_displayed_fields = ['account_type_id','lang'];



  public field_access = new Map([
    ['id', ModelSettings.FIELD_HIDDEN],
    ['account_type_id', ModelSettings.FIELD_INPUT],
    ['lang', ModelSettings.FIELD_HIDDEN],
    ['account_type_name', ModelSettings.FIELD_INPUT]

    ]);


  getTableRecords(userSite: string) {
    this.loadAccountType(userSite);
   // return this.tableRecords;
  }


  loadAccountType(userSite: string) {

    this.ormAccountingService.getRecords("account_type",["*"],null,"(lang='"+this.dataDispatch.langToISO3()+"')")
      .pipe(
        map((data: ResourceResponse<IAccountType>) => {
          // Flip flag to show that loading has finished.

          return data.resource;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          this._tableRecords = usages as IAccountType[];

        });
  }


}
