import { DataTableInfo } from './model-table-info';
import { ModelSettings } from './model-settings';

export class IAccount {
  id ?: string;
  organisation_id ?: string;
  account_type_id ?: string;
  account_id ?: string;
  account_short_code ?: string;
  account_name ?: string;
  auxiliary_general_account_id ?: string;
  letter ?: string;
  tag ?: string;
  account_group_id ?: string;
  account_subgroup_id ?: string;
  account_old_id ?: string;
  account_export_id ?: string;
  dc ?: string;
  debit ?: number;
  credit ?: number;
  analytic_section_id ?: string;
  analytic_group_id ?: string;
  analytic_code_id ?: string;
  analytic_part1 ?: string;
  analytic_part2 ?: string;
  analytic_part3 ?: string;
  analytic_part4 ?: string;
  analytic_part5 ?: string;
  budget_section_id ?: string;
  budget_group_id ?: string;
  budget_code_id ?: string;
  vat_id ?: string;
  tax_id ?: string;
  financial_statement_page ?: string;
  financial_statement_item ?: string;
  created_by ?: string;
  updated_by ?: string;
  deleted_by ?: string;
  created_on ?: string;
  updated_on ?: string;
  deleted_on ?: string;
  created_on_station ?: string;
  updated_on_station ?: string;
  deleted_on_station ?: string;
  origin_code ?: string;
  origin_id ?: string;
  origin_date ?: string;
  status ?: string;
  export ?: string;
}

export class AccountTable extends DataTableInfo {


  //public ormService : OrmBrsService;

  public id_field = 'account_id';
  public main_displayed_field = 'account_name';
  public additional_displayed_fields = ['organisation_id','account_type_id','organisation_id'];
  public fields_hidden_in_table = ['account_type_id','description','account_export_id','account_old_id','account_subgroup_id','account_id','account_group_id','created_on','updated_on','financial_statement_page','financial_statement_item','budget_section_id','budget_code_id','budget_group_id'];
  public non_editable_fields = ['created_on','updated_on'];

  public field_access = new Map([
    ['id', ModelSettings.FIELD_HIDDEN],
    ['organisation_id', ModelSettings.FIELD_HIDDEN],
    ['account_type_id', ModelSettings.FIELD_SELECTION],
    ['account_id', ModelSettings.FIELD_HIDDEN],
    ['account_short_code', ModelSettings.FIELD_INPUT],
    ['account_name', ModelSettings.FIELD_INPUT],
    ['auxiliary_general_account_id', ModelSettings.FIELD_INPUT],
    ['letter', ModelSettings.FIELD_INPUT],
    ['tag', ModelSettings.FIELD_INPUT],
    ['account_group_id', ModelSettings.FIELD_INPUT],
    ['account_subgroup_id', ModelSettings.FIELD_INPUT],
    ['account_old_id', ModelSettings.FIELD_INPUT],
    ['account_export_id', ModelSettings.FIELD_INPUT],
    ['dc', ModelSettings.FIELD_INPUT],
    ['debit', ModelSettings.FIELD_INPUT],
    ['credit', ModelSettings.FIELD_INPUT],
    ['analytic_section_id', ModelSettings.FIELD_INPUT],
    ['analytic_group_id', ModelSettings.FIELD_INPUT],
    ['analytic_code_id', ModelSettings.FIELD_INPUT],
    ['analytic_part1', ModelSettings.FIELD_INPUT],
    ['analytic_part2', ModelSettings.FIELD_INPUT],
    ['analytic_part3', ModelSettings.FIELD_INPUT],
    ['analytic_part4', ModelSettings.FIELD_INPUT],
    ['analytic_part5', ModelSettings.FIELD_INPUT],
    ['budget_section_id', ModelSettings.FIELD_INPUT],
    ['budget_code_id', ModelSettings.FIELD_INPUT],
    ['budget_group_id', ModelSettings.FIELD_INPUT],
    ['vat_id', ModelSettings.FIELD_SELECTION],
    ['tax_id', ModelSettings.FIELD_SELECTION],
    ['financial_statement_page', ModelSettings.FIELD_INPUT],
    ['financial_statement_item', ModelSettings.FIELD_INPUT],
    ['created_by', ModelSettings.FIELD_HIDDEN],
    ['updated_by', ModelSettings.FIELD_HIDDEN],
    ['deleted_by', ModelSettings.FIELD_HIDDEN],
    ['created_on', ModelSettings.FIELD_DATE],
    ['updated_on', ModelSettings.FIELD_DATE],
    ['deleted_on', ModelSettings.FIELD_HIDDEN],
    ['created_on_station', ModelSettings.FIELD_HIDDEN],
    ['updated_on_station', ModelSettings.FIELD_HIDDEN],
    ['deleted_on_station', ModelSettings.FIELD_HIDDEN],
    ['origin_code', ModelSettings.FIELD_HIDDEN],
    ['origin_id', ModelSettings.FIELD_HIDDEN],
    ['origin_date', ModelSettings.FIELD_HIDDEN],
    ['status', ModelSettings.FIELD_INPUT],
    ['export', ModelSettings.FIELD_INPUT]

    ]);

    public field_reference_table = new Map([
      [ "account_type_id", "account_type" ],
      [ "vat_id", "vat" ],
      [ "tax_id", "tax" ]
    ]);

    public field_foreign_key = new Map([
      [ "account_type_id", "account_type_id" ],
      [ "vat_id", "vat_id" ],
      [ "tax_id", "id" ]
    ]);


  public linked_data = [
                        ];

  getTableRecords(userSite: string) {
    //this.loadIataTypeBRecipient(userSite);
    return this.tableRecords;
  }

  /*
  loadIataTypeBRecipient(userSite: string) {

    this.ormService.getRecords("iata_typeb_recipient",null,null,"(airport_iata_code='"+userSite+"')")
      .pipe(
        map((data: ResultResponse<IIataTypeBRecipient>) => {
          // Flip flag to show that loading has finished.

          return data.result;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          this._tableRecords = usages as IIataTypeBRecipient[];

        });
  }
  */

}
