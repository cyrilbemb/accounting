export class AccountingParameters {
}
import { DataTableInfo } from './model-table-info';
import { ModelSettings } from './model-settings';

export class IAccountingParameters {
  organisation_id?:   string;
  name?:          string;
  description?:          string;
  value?:                string;
}

export class AccountingParametersTable extends DataTableInfo {


  //public ormService : OrmBrsService;

  public id_field = 'name';
  public main_displayed_field = 'airport_iata_code';
  public additional_displayed_fields = ['description'];

  public field_access = new Map([
    ['organisation_id', ModelSettings.FIELD_HIDDEN],
    ['name', ModelSettings.FIELD_INPUT],
    ['description', ModelSettings.FIELD_INPUT],
    ['value', ModelSettings.FIELD_INPUT]

    ]);

  public linked_data = [
                        ];

  getTableRecords(userSite: string) {
    //this.loadIataTypeBRecipient(userSite);
    return this.tableRecords;
  }

  /*
  loadIataTypeBRecipient(userSite: string) {

    this.ormService.getRecords("iata_typeb_recipient",null,null,"(airport_iata_code='"+userSite+"')")
      .pipe(
        map((data: ResultResponse<IIataTypeBRecipient>) => {
          // Flip flag to show that loading has finished.

          return data.result;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          this._tableRecords = usages as IIataTypeBRecipient[];

        });
  }
  */

}
