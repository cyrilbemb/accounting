import { DataTableInfo } from './model-table-info';
import { ModelSettings } from './model-settings';
import { formatDate } from "@angular/common";

export class IAccountingPeriod {
  accounting_period_id?:    string;
  organisation_id?:   string;
  accounting_period_code?:          string;

  accounting_period_from_date_?:          Date;
  accounting_period_to_date_?:                Date;
  accounting_period_closed_on?:              Date;
  accounting_period_closed?: Date;
  datediff_period?: number;

  get accounting_period_from_date(){
    console.log('tst');
    return  new Date(this.accounting_period_from_date_);
  }
  set accounting_period_from_date(value : any){
    this.accounting_period_from_date_ = new Date(value);
  }

  get accounting_period_to_date(){
    return  new Date(this.accounting_period_to_date_);
  }
  set accounting_period_to_date(value : any){
    this.accounting_period_to_date_ = new Date(value);
  }
}

export class AccountingPeriodTable extends DataTableInfo {


  //public ormService : OrmBrsService;

  public id_field = 'accounting_period_code';
  public main_displayed_field = 'accounting_period_code';
  public additional_displayed_fields = ['accounting_period_id','accounting_period_from_date', 'accounting_period_to_date','datediff_period'];


  public non_editable_fields: string[] = ['datediff_period','accounting_period_closed','accounting_period_closed_on'];


  public field_access = new Map([
    ['accounting_period_id', ModelSettings.FIELD_HIDDEN],
    ['organisation_id', ModelSettings.FIELD_HIDDEN],
    ['accounting_period_from_date', ModelSettings.FIELD_DATE],
    ['accounting_period_code', ModelSettings.FIELD_INPUT],
    ['accounting_period_to_date', ModelSettings.FIELD_DATE],
    ['accounting_period_closed_on', ModelSettings.FIELD_DATE],
    ['accounting_period_closed', ModelSettings.FIELD_CHECKBOX],
    ['datediff_period', ModelSettings.FIELD_INPUT],

    ]);

  public linked_data = [
                        "accounting_period_from_date<DATE<accounting_period_to_date",
                        "accounting_period_code|REG|^[A-B0-9\-]+$"
                      ];


  getTableRecords(userSite: string) {
    //this.loadIataTypeBRecipient(userSite);
    return this.tableRecords;
  }

  /*
  loadIataTypeBRecipient(userSite: string) {

    this.ormService.getRecords("iata_typeb_recipient",null,null,"(airport_iata_code='"+userSite+"')")
      .pipe(
        map((data: ResultResponse<IIataTypeBRecipient>) => {
          // Flip flag to show that loading has finished.

          return data.result;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          this._tableRecords = usages as IIataTypeBRecipient[];

        });
  }
  */

}
