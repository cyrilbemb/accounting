import { DataTableInfo } from './model-table-info';

import { ModelSettings } from './model-settings';

export class IDcList {

  public static DEBIT_TYPE_VALUE : string = 'D';
  public static CREDIT_TYPE_VALUE : string = 'C';

  type : string;
  name : string;

}

export class DcListTable extends DataTableInfo {


  //public ormService : OrmAccountingService;
  //public dataDispatch : DataDispatchService;


  public id_field = 'type';
  public main_displayed_field = 'name';
  public additional_displayed_fields = [];



  public field_access = new Map([
    ['type', ModelSettings.FIELD_HIDDEN],
    ['name', ModelSettings.FIELD_INPUT]

    ]);


  getTableRecords(userSite: string) {
    this.loadJournalType(userSite);
    //return this.tableRecords;
  }


  loadJournalType(userSite: string) {

    let debit : IDcList = {
      type : IDcList.DEBIT_TYPE_VALUE,
      name : this.translateService.instant('dc_list.debit')
    };

    let credit : IDcList = {
      type : IDcList.CREDIT_TYPE_VALUE,
      name : this.translateService.instant('dc_list.credit')
    };

    this.tableRecords.push(debit,credit);
  }


}
