export class IImportHeader {

  public static IMPORT_JSON :string = 'json';
  public static IMPORT_XML : string = 'xml';
  public static IMPORT_CUSTOM : string = 'text';
  public static IMPORT_CSV :string = 'csv';
  public static IMPORT_TSV : string = 'tsv';


  uuid ?: string;
  import_name ?: string;
  import_format ?: string;
  import_field_separator ?: string;
  import_end_of_line ?: string;
  import_row_ignore ?: number;
  import_row_preview ?: number = 1;
  import_source_filename ?: string;
  import_date_format ?: string;
  import_datetime_format ?: string;
  status ?: number;
}
