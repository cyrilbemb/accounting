import { DataTableInfo } from './model-table-info';

import { ModelSettings } from './model-settings';

import { map, catchError } from 'rxjs/operators';

import { ResourceResponse } from 'eaf-lib';

import { IJournalType } from './journal-type';

import { of } from 'rxjs';

export class IJournalEntry {
  uuid?:                         string;
  journal_piece_uid ?: string;
  journal_entry_item?:           string;
  journal_entry_line?:           number;
  journal_entry_code?:           string;
  journal_entry_comment?:        string;
  account_type_id?:              number;
  account_id?:                   string;
  account_address_id?:           number;
  auxiliary_general_account_id?: string;
  counterpart_account_id?:       string;
  dc?:                           string;
  currency_amount?:              number;
  amount?:                       number;
  debit?:                        number;
  credit?:                       number;
  vat_id?:                       string;
  vat_amount?:                   number;
  tax_id?:                       string;
  tax_amount?:                   number;
  letter?:                       string;
  mark?:                         string;
  validated?:                    number;
  temporary_journal_entry?:      number;
  budget_section_id?:            string;
  budget_group_id?:              string;
  budget_code_id?:               string;
  analytic_section_id?:          string;
  analytic_group_id?:            string;
  analytic_code_id?:             string;
  quantity?:                     number;
  weight?:                       number;
  payment_mode_id?:              string;
  duedate?:                      Date;
  ok_for_payment?:               number;
  litigation?:                   number;
  nationality_id?:               string;
  accounting_code_id?:           string;
  created_by?:                   string;
  updated_by?:                   string;
  deleted_by?:                   string;
  created_on?:                   Date;
  updated_on?:                   Date;
  deleted_on?:                   Date;
  created_on_station?:           string;
  updated_on_station?:           string;
  deleted_on_station?:           string;
  origin_code?:                  string;
  origin_id?:                    number;
  origin_date?:                  string;
  status?:                       number;
  export?:                       number;

}

export class JournalEntryTable extends DataTableInfo {


  //public ormService : OrmAccountingService;
  //public dataDispatch : DataDispatchService;


  public id_field = 'uuid';
  public main_displayed_field = 'journal_entry_item';
  public additional_displayed_fields = ['journal_piece_uid','amount','journal_entry_due_date'];
  public fields_hidden_in_table = ['journal_type_id',
                                    'journal_id',
                                    'origin_code',
                                    'origin_id',
                                    'origin_date',
                                    'deleted_on',
                                    'created_on',
                                    'updated_on',
                                    'updated_on_station',
                                    'created_on_station',
                                    'updated_by',
                                    'created_by',
                                    'auxiliary_general_account_id',
                                    'validated',
                                    'budget_code_id',
                                    'nationality_id',
                                    'analytic_section_id',
                                    'journal_entry_date',
                                    'journal_entry_value_date',
                                    'dc',
                                    'amount'
                                  ];

  public field_reference_table = new Map([
    ['journal_piece_uid','journal_piece'],
    ["account_type_id","account_type"],
    ["nationality_id","nationality"],
    ["dc","dc_list"]
  ]);

  public field_foreign_key = new Map([
    ['journal_piece_uid','uuid'],
    ["account_type_id","account_type_id"],
    ["nationality_id","nationality_id"],
    ["dc","type"]

  ]);

  public field_access = new Map([
    ['uuid', ModelSettings.FIELD_HIDDEN],
    ['journal_entry_item', ModelSettings.FIELD_HIDDEN],
    ['journal_entry_line', ModelSettings.FIELD_INPUT],
     ['journal_entry_code', ModelSettings.FIELD_HIDDEN],
    ['journal_entry_comment', ModelSettings.FIELD_INPUT],
     ['account_type_id', ModelSettings.FIELD_HIDDEN],
    ['account_id', ModelSettings.FIELD_INPUT],
     ['account_address_id', ModelSettings.FIELD_HIDDEN],
    ['auxiliary_general_account_id', ModelSettings.FIELD_INPUT],
     ['counterpart_account_id', ModelSettings.FIELD_HIDDEN],
    ['dc',ModelSettings.FIELD_SELECTION],
     ['currency_amount', ModelSettings.FIELD_HIDDEN],
    ['amount', ModelSettings.FIELD_INPUT],
    ['debit', ModelSettings.FIELD_HIDDEN],
    ['credit', ModelSettings.FIELD_HIDDEN],
     ['vat_id', ModelSettings.FIELD_HIDDEN],
     ['vat_amount', ModelSettings.FIELD_HIDDEN],
     ['tax_id', ModelSettings.FIELD_HIDDEN],
     ['tax_amount', ModelSettings.FIELD_HIDDEN],
     ['letter', ModelSettings.FIELD_HIDDEN],
     ['mark', ModelSettings.FIELD_HIDDEN],
    ['validated', ModelSettings.FIELD_INPUT],
     ['temporary_journal_entry', ModelSettings.FIELD_HIDDEN],
     ['budget_section_id', ModelSettings.FIELD_HIDDEN],
     ['budget_group_id', ModelSettings.FIELD_HIDDEN],
     ['budget_code_id', ModelSettings.FIELD_INPUT],
    ['analytic_section_id', ModelSettings.FIELD_INPUT],
     ['analytic_group_id', ModelSettings.FIELD_HIDDEN],
     ['analytic_code_id', ModelSettings.FIELD_HIDDEN],
     ['quantity', ModelSettings.FIELD_HIDDEN],
     ['weight', ModelSettings.FIELD_HIDDEN],
     ['payment_mode_id', ModelSettings.FIELD_HIDDEN],
     ['duedate', ModelSettings.FIELD_DATE],
     ['ok_for_payment', ModelSettings.FIELD_HIDDEN],
     ['litigation', ModelSettings.FIELD_HIDDEN],
    ['nationality_id', ModelSettings.FIELD_SELECTION],
     ['accounting_code_id', ModelSettings.FIELD_HIDDEN],
    ['created_by', ModelSettings.FIELD_HIDDEN],
    ['updated_by', ModelSettings.FIELD_HIDDEN],
     ['deleted_by', ModelSettings.FIELD_HIDDEN],
    ['created_on', ModelSettings.FIELD_HIDDEN],
    ['updated_on', ModelSettings.FIELD_HIDDEN],
     ['deleted_on', ModelSettings.FIELD_HIDDEN],
    ['created_on_station', ModelSettings.FIELD_HIDDEN],
    ['updated_on_station', ModelSettings.FIELD_HIDDEN],
     ['deleted_on_station', ModelSettings.FIELD_HIDDEN],
    ['origin_code', ModelSettings.FIELD_INPUT],
    ['origin_id', ModelSettings.FIELD_INPUT],
    ['origin_date', ModelSettings.FIELD_DATE],
     ['status', ModelSettings.FIELD_HIDDEN],
     ['export', ModelSettings.FIELD_HIDDEN]

    ]);

    public field_width = new Map([
      ["journal_entry_comment", 40],
      ["journal_entry_line", 3],
      ["debit", 7],
      ["credit", 7]
    ]);

    public linked_data = [
      "amount|REG|^\\d+$"
    ];

    public non_editable_fields = ['journal_entry_item','journal_piece_uid'];

  getTableRecords(userSite: string) {
    //this.loadJournalType(userSite);
    return this.tableRecords;
  }


  loadJournalType(userSite: string) {

    this.ormAccountingService.getRecords("journal_type",["*"],null,"(lang='"+this.dataDispatch.langToISO3()+"')")
      .pipe(
        map((data: ResourceResponse<IJournalType>) => {
          // Flip flag to show that loading has finished.

          return data.resource;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          console.log("usages",usages);
          this._tableRecords = usages as IJournalType[];

        });
  }


}
