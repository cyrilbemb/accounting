import { DataTableInfo } from './model-table-info';
import { ModelSettings } from './model-settings';
import { map, catchError } from 'rxjs/operators';
import { ResultResponse } from 'eaf-lib';
import { of } from 'rxjs';

export class IJournalPiece {
  uuid?:                     string;
  organisation_id?:          string;
  site_id?:                  string;
  accounting_period_id?:     string;
  accounting_period_number?: number;
  journal_id?:               string;
  journal_piece_id ?: string;
  financial_category_id?:    string;
  journal_entry_date?:       Date;
  journal_entry_value_date?: Date;
  journal_entry_due_date?:   Date;
  currency_id?:              string;
  currency_rate?:            number;
  ok_for_payment?:           number;
  payment_mode_id?:          string;
  duedate?:                  Date;
  litigation?:               number;
  nationality_id?:           string;
  representative_id?:        string;
  created_by?:               string;
  updated_by?:               string;
  deleted_by?:               string;
  created_on?:               string;
  updated_on?:               string;
  deleted_on?:               string;
  created_on_station?:       string;
  updated_on_station?:       string;
  deleted_on_station?:       string;
  origin_code?:              string;
  origin_id?:                string;
  origin_date?:              Date;
  status?:                   number;
  export?:                   number;
}

export class JournalPieceTable extends DataTableInfo {


  //public ormService : OrmBrsService;

  public id_field = 'uuid';
  public main_displayed_field = 'accounting_period_number';
  public additional_displayed_fields = ['organisation_id'];
  public non_editable_fields = ['journal_piece_id'];

  public field_access = new Map([
    ['uuid', ModelSettings.FIELD_HIDDEN],
    ['organisation_id', ModelSettings.FIELD_HIDDEN],
    ['site_id', ModelSettings.FIELD_HIDDEN],
    ['accounting_period_id', ModelSettings.FIELD_HIDDEN],
    ['accounting_period_number', ModelSettings.FIELD_HIDDEN],
    ['journal_id', ModelSettings.FIELD_SELECTION],
    ['journal_piece_id', ModelSettings.FIELD_HIDDEN],
    ['financial_category_id', ModelSettings.FIELD_INPUT],
    ['journal_entry_date', ModelSettings.FIELD_DATE],
    ['journal_entry_value_date', ModelSettings.FIELD_DATE],
    ['journal_entry_due_date', ModelSettings.FIELD_DATE],
    ['currency_id', ModelSettings.FIELD_INPUT],
    ['currency_rate', ModelSettings.FIELD_INPUT],
    ['ok_for_payment', ModelSettings.FIELD_CHECKBOX],
    ['payment_mode_id', ModelSettings.FIELD_INPUT],
    ['duedate', ModelSettings.FIELD_HIDDEN],
    ['litigation', ModelSettings.FIELD_INPUT],
    ['nationality_id', ModelSettings.FIELD_SELECTION],
    ['representative_id', ModelSettings.FIELD_INPUT],
    ['created_by', ModelSettings.FIELD_HIDDEN],
    ['updated_by', ModelSettings.FIELD_HIDDEN],
    ['deleted_by', ModelSettings.FIELD_HIDDEN],
    ['created_on', ModelSettings.FIELD_HIDDEN],
    ['updated_on', ModelSettings.FIELD_HIDDEN],
    ['deleted_on', ModelSettings.FIELD_HIDDEN],
    ['created_on_station', ModelSettings.FIELD_HIDDEN],
    ['updated_on_station', ModelSettings.FIELD_HIDDEN],
    ['deleted_on_station', ModelSettings.FIELD_HIDDEN],
    ['origin_code', ModelSettings.FIELD_HIDDEN],
    ['origin_id', ModelSettings.FIELD_HIDDEN],
    ['origin_date', ModelSettings.FIELD_HIDDEN],
    ['status', ModelSettings.FIELD_INPUT],
    ['export', ModelSettings.FIELD_INPUT]

    ]);

  public linked_data = [
                        ];

  public field_reference_table = new Map([
    [ "journal_id", "journal" ],
    ['nationality_id','nationality']
  ]);

  public field_foreign_key = new Map([
    [ "journal_id", "id" ],
    ['nationality_id','nationality_id']
  ]);

  getTableRecords(userSite: string) {
    this.loadJournalPiece(userSite);
    //return this.tableRecords;
  }


  loadJournalPiece(userSite: string) {

    this.ormAccountingService.getRecords("journal_piece",null,null,"((organisation_id='EAA') AND (site_id='EAF'))")
      .pipe(
        map((data: ResultResponse<IJournalPiece>) => {
          // Flip flag to show that loading has finished.

          return data.result;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          this._tableRecords = usages as IJournalPiece[];

        });
  }


}

