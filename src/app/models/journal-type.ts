import { DataTableInfo } from './model-table-info';

import { ModelSettings } from './model-settings';
import { OrmAccountingService } from '../services/db-table-service/orm-accounting.service';
import { map, catchError } from 'rxjs/operators';
import { ResultResponse, ResourceResponse } from 'eaf-lib';
import { of } from 'rxjs';
import { DataDispatchService } from '../services/data-dispatch.service';

export class IJournalType {
  id?:    string;
  journal_type_id?:   string;
  lang?:          string;

  journal_type_name?:          string;

}

export class JournalTypeTable extends DataTableInfo {


  //public ormService : OrmAccountingService;
  //public dataDispatch : DataDispatchService;


  public id_field = 'journal_type_id';
  public main_displayed_field = 'journal_type_name';
  public additional_displayed_fields = ['journal_type_id','lang'];



  public field_access = new Map([
    ['id', ModelSettings.FIELD_HIDDEN],
    ['journal_type_id', ModelSettings.FIELD_INPUT],
    ['lang', ModelSettings.FIELD_HIDDEN],
    ['journal_type_name', ModelSettings.FIELD_INPUT]

    ]);


  getTableRecords(userSite: string) {
    this.loadJournalType(userSite);
    //return this.tableRecords;
  }


  loadJournalType(userSite: string) {

    this.ormAccountingService.getRecords("journal_type",["*"],null,"(lang='"+this.dataDispatch.langToISO3()+"')")
      .pipe(
        map((data: ResourceResponse<IJournalType>) => {
          // Flip flag to show that loading has finished.

          return data.resource;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          console.log("usages",usages);
          this._tableRecords = usages as IJournalType[];

        });
  }


}
