import { DataTableInfo } from './model-table-info';

import { ModelSettings } from './model-settings';
import { map, catchError } from 'rxjs/operators';
import { ResultResponse, ResourceResponse } from 'eaf-lib';
import { of } from 'rxjs';

export class IJournal {

  id ?: string;
  organisation_id ?: string;

  journal_id ?: string;
  journal_name ?: string;
  journal_type_id ?: string;

  account_id ?: string;
  idcounter_id ?: string;
  dc ?: string;
}

export class JournalTable extends DataTableInfo {


  //public ormService : OrmBrsService;

  public id_field = 'journal_id';
  public main_displayed_field = 'journal_name';
  public additional_displayed_fields = ['organisation_id'];
  public fields_hidden_in_table = ['journal_type_id'];

  public field_access = new Map([
    ['id', ModelSettings.FIELD_HIDDEN],
    ['journal_id', ModelSettings.FIELD_INPUT],
    ['journal_name', ModelSettings.FIELD_INPUT],
    ['journal_type_id', ModelSettings.FIELD_SELECTION],
    ['account_id', ModelSettings.FIELD_HIDDEN],
    ['organisation_id', ModelSettings.FIELD_HIDDEN],
    ['idcounter_id', ModelSettings.FIELD_HIDDEN],
    ['dc', ModelSettings.FIELD_INPUT]

    ]);

  public linked_data = [
                        ];

  public field_reference_table = new Map([
    [ "journal_type_id", "journal_type" ]
  ]);

  public field_foreign_key = new Map([
    [ "journal_type_id", "journal_type_id" ]
  ]);

  getTableRecords(userSite: string) {
    this.loadJournal(userSite);
    //return this.tableRecords;
  }


  loadJournal(userSite: string){
    this.ormAccountingService.getRecords("journal",["*"],null,"(organisation_id='EAA')")
    .pipe(
      map((data: ResourceResponse<IJournal>) => {
        // Flip flag to show that loading has finished.

        return data.resource;
      }),
      catchError(() => {
        return of([]);
      })
    )
    .subscribe(
      (usages) => {
        // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
        console.log("usages",usages);
        this._tableRecords = usages as IJournal[];

      });
  }

}
