export class ModelSettings {

    // Item grants
    public static FIELD_HIDDEN: number = 0;
    public static FIELD_INPUT: number = 2;
    public static FIELD_SELECTION: number = 4;
    public static FIELD_CHECKBOX: number = 8;
    public static FIELD_TEXTAREA: number = 16;
    public static FIELD_DATE: number = 32;

    get FIELD_HIDDEN() {
        return ModelSettings.FIELD_HIDDEN;
    }

    get FIELD_INPUT() {
        return ModelSettings.FIELD_INPUT;
    }

    get FIELD_SELECTION() {
        return ModelSettings.FIELD_SELECTION;
    }

    get FIELD_TEXTAREA() {
        return ModelSettings.FIELD_TEXTAREA;
    }
    get FIELD_DATE() {
      return ModelSettings.FIELD_DATE;
  }
}
