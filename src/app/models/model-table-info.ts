import { Inject, Optional, Injector } from '@angular/core';
import { ReflectiveInjector, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { OrmAccountingService } from '../services/db-table-service/orm-accounting.service';
import { DataDispatchService } from '../services/data-dispatch.service';
import { OrmAmdirectoryService } from '../services/db-table-service/orm-amdirectory.service';

export abstract class DataTableInfo {

  public table_item: any;

  //public userSiteId: string = '';

  public id_field: string = "";
  public main_displayed_field: string = "";
  public additional_displayed_fields: string[] = [];
  public mandatory_fields: string[] = [];

  public linked_data: string[]= [];

  public non_editable_fields: string[] = [];

  public fields_hidden_in_table: string[] = [];

  public field_access = new Map();
  public field_width = new Map([]);
  public field_validator = new Map([]);

  public field_reference_table = new Map();
  public field_foreign_key = new Map();

  protected _tableRecords: any[] = [];

  public get tableRecords() {
    return this._tableRecords;
  }

  public set tableRecords(values:any[]) {
    this._tableRecords = values;
  }

  constructor(
    public ormAccountingService: OrmAccountingService,
    public ormAmdirectoryService: OrmAmdirectoryService,
    public dataDispatch : DataDispatchService,
    public translateService: TranslateService) {
    /*
    const injector = Injector.create({
      providers: [
        { provide: BrsService, useClass: BrsService, deps: []},
      ]
    });
    this.brsService = injector.get(BrsService);
    */
  }

  abstract getTableRecords(userSite : string);

}




