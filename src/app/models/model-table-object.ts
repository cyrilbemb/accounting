
import { TranslateService } from '@ngx-translate/core';
import { AccountingParametersTable } from './accounting-parameters';
import { AccountingPeriodTable } from './accounting-period';
import { DataTableInfo } from './model-table-info';
import { OrmAccountingService } from '../services/db-table-service/orm-accounting.service';
import { NationalityTable } from './nationality';
import { JournalTypeTable } from './journal-type';
import { AccountTypeTable } from './account-type';
import { AccountTable } from './account';
import { JournalTable } from './journal';
import { DataDispatchService } from '../services/data-dispatch.service';
import { OrmAmdirectoryService } from '../services/db-table-service/orm-amdirectory.service';
import { TaxTypeTable } from './tax';
import { VatTypeTable } from './vat';
import { JournalEntryTable } from './journal-entry';
import { DcListTable } from './dc-list';
import { JournalPieceTable } from './journal-piece';

export class ModelTableObject {
  /*
    public static db_table_models = new Map([
        [ "brs_flight_container", new FlightContainerTable() ],
        [ "brs_container_type", new ContainerTypeTable() ],
        [ "brs_container_usage", new ContainerUsageTable() ],
        [ "brs_container_content", new ContainerContentTable() ]
    ]);
    */

  public static getTableModel(table_name: string, ormService: OrmAmdirectoryService, ormAccounting : OrmAccountingService,dataService: DataDispatchService, translateService?: TranslateService): DataTableInfo {
    switch (table_name) {
      case "accounting_parameter":
        return new AccountingParametersTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "accounting_period":
        // Virtual table for Application
        return new AccountingPeriodTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "nationality":
        // Virtual table for Application
        return new NationalityTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "journal_type":
        // Virtual table for Application
        return new JournalTypeTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "account_type":
        // Virtual table for Application
        return new AccountTypeTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "account":
        // Virtual table for Application
        return new AccountTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "journal":
        // Virtual table for Application
        return new JournalTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "tax":
        // Virtual table for Application
        return new TaxTypeTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "vat":
        // Virtual table for Application
        return new VatTypeTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "journal_entry":
        // Virtual table for Application
        return new JournalEntryTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "journal_piece":
        // Virtual table for Application
        return new JournalPieceTable(ormAccounting,ormService, dataService,translateService);
        break;
      case "dc_list":
        // Virtual table for Application
        return new DcListTable(ormAccounting,ormService, dataService,translateService);
        break;
    default:
      return undefined;
      break;
    }
  }
}
