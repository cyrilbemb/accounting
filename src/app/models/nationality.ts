import { DataTableInfo } from './model-table-info';
import { ModelSettings } from './model-settings';
import { map, catchError } from 'rxjs/operators';
import { ResourceResponse } from 'eaf-lib';
import { of } from 'rxjs';

export class INationality {
  nationality_id?:    string;
  organisation_id?:   string;
  nationality_code?:          string;

  nationality_name?:          string;
  nationality_priority?:                number;
  nationality_import_code?:              string;
  nationality_export_code?:              string;
  nationality_vat?: number;
  accounting_code?: string;
  analytic_section_code?: string;
}

export class NationalityTable extends DataTableInfo {


  //public ormService : OrmBrsService;

  public id_field = 'nationality_code';
  public main_displayed_field = 'nationality_name';
  public additional_displayed_fields = ['nationality_id','accounting_code'];

  public field_access = new Map([
    ['nationality_id', ModelSettings.FIELD_HIDDEN],
    ['organisation_id', ModelSettings.FIELD_HIDDEN],
    ['nationality_code', ModelSettings.FIELD_INPUT],
    ['nationality_name', ModelSettings.FIELD_INPUT],
    ['nationality_priority', ModelSettings.FIELD_INPUT],
    ['nationality_import_code', ModelSettings.FIELD_INPUT],
    ['nationality_export_code', ModelSettings.FIELD_INPUT],
    ['nationality_vat', ModelSettings.FIELD_INPUT],
    ['accounting_code', ModelSettings.FIELD_INPUT],
    ['analytic_section_code', ModelSettings.FIELD_INPUT]
    ]);

  public linked_data = [
                      ];


  getTableRecords(userSite: string) {
    this.loadNationalities(userSite);
    //return this.tableRecords;
  }

  loadNationalities(userSite: string){
    this.ormAccountingService.getRecords("nationality",["*"],null,"(organisation_id='EAA')")
    .pipe(
      map((data: ResourceResponse<INationality>) => {
        // Flip flag to show that loading has finished.

        return data.resource;
      }),
      catchError(() => {
        return of([]);
      })
    )
    .subscribe(
      (usages) => {
        // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
        console.log("usages",usages);
        this._tableRecords = usages as INationality[];

      });
  }

}
