import { DataTableInfo } from './model-table-info';
import { ModelSettings } from './model-settings';
import { map, catchError } from 'rxjs/operators';
import { ResourceResponse } from 'eaf-lib';
import { IAccountType } from './account-type';
import { of } from 'rxjs';

export class ITax {
  id?:    string;
  country_id?:   string;
  tax_id?:          number;
  tax_name?:    string;
  vat_id?:   number;
  tax_rate?:          number;
  tax_calculation_base?:    string;
  account_id?:   string;
  tax_external_code?:          string;

}

export class TaxTypeTable extends DataTableInfo {



  public id_field = 'tax_id';
  public main_displayed_field = 'tax_name';
  public additional_displayed_fields = ['vat_id','country_id'];
  public fields_hidden_in_table = ['tax_id','tax_rate','tax_calculation_base','tax_external_code'];



  public field_access = new Map([
    ['id', ModelSettings.FIELD_HIDDEN],
    ['country_id', ModelSettings.FIELD_HIDDEN],
    ['tax_id', ModelSettings.FIELD_INPUT],
    ['tax_name', ModelSettings.FIELD_INPUT],
    ['vat_id', ModelSettings.FIELD_SELECTION],
    ['tax_rate', ModelSettings.FIELD_INPUT],
    ['tax_calculation_base', ModelSettings.FIELD_INPUT],
    ['account_id', ModelSettings.FIELD_INPUT],
    ['tax_external_code', ModelSettings.FIELD_INPUT]

    ]);

    public field_reference_table = new Map([
      [ "vat_id", "vat" ]
    ]);

    public field_foreign_key = new Map([
      [ "vat_id", "vat_id" ]
    ]);

  getTableRecords(userSite: string) {
    this.loadTaxType(userSite);
   // return this.tableRecords;
  }


  loadTaxType(userSite: string) {

    this.ormAmdirectoryService.getRecords("tax",["*"])
      .pipe(
        map((data: ResourceResponse<ITax>) => {
          // Flip flag to show that loading has finished.

          return data.resource;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          this._tableRecords = usages as ITax[];

        });
  }


}
