import { DataTableInfo } from './model-table-info';

import { ModelSettings } from './model-settings';

import { map, catchError } from 'rxjs/operators';

import { ResourceResponse } from 'eaf-lib';

import { ITax } from './tax';

import { of } from 'rxjs';

export class IVat {
  id?:    string;
  country_id?:   string;
  vat_id?:          number;
  vat_rate?:    string;
  account_id?:   number;
  vat_external_code?:          number;
  vat_old_rate?:    string;
  vat_old_rate_until?:   string;

}

export class VatTypeTable extends DataTableInfo {



  public id_field = 'vat_id';
  public main_displayed_field = 'vat_rate';
  public additional_displayed_fields = ['account_id','country_id'];
  public fields_hidden_in_table = ['vat_external_code','vat_old_rate','vat_old_rate_until'];


  public field_access = new Map([
    ['id', ModelSettings.FIELD_HIDDEN],
    ['country_id', ModelSettings.FIELD_HIDDEN],
    ['vat_id', ModelSettings.FIELD_INPUT],
    ['vat_rate', ModelSettings.FIELD_INPUT],
    ['account_id', ModelSettings.FIELD_INPUT],
    ['vat_external_code', ModelSettings.FIELD_INPUT],
    ['vat_old_rate', ModelSettings.FIELD_INPUT],
    ['vat_old_rate_until', ModelSettings.FIELD_INPUT]

    ]);

  getTableRecords(userSite: string) {
    this.loadVatType(userSite);
   // return this.tableRecords;
  }


  loadVatType(userSite: string) {

    this.ormAmdirectoryService.getRecords("vat",["*"])
      .pipe(
        map((data: ResourceResponse<IVat>) => {
          // Flip flag to show that loading has finished.

          return data.resource;
        }),
        catchError(() => {
          return of([]);
        })
      )
      .subscribe(
        (usages) => {
          // ContainerUsageTable.tableRecords = usages as BrsContainerUsage[];
          this._tableRecords = usages as IVat[];

        });
  }


}

