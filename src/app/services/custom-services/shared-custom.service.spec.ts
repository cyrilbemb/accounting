/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SharedCustomService } from './shared-custom.service';

describe('Service: SharedCustom', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedCustomService]
    });
  });

  it('should ...', inject([SharedCustomService], (service: SharedCustomService) => {
    expect(service).toBeTruthy();
  }));
});
