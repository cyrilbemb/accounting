import { Injectable, Optional, Inject } from '@angular/core';
import { BaseDfApiService, BASE_PATH, Configuration, CustomHttpUrlEncodingCodec } from 'eaf-lib';
import { HttpClient, HttpResponse, HttpEvent, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

enum apiMethod {
  get,
  post,
  put,
  patch,
  delete
}

@Injectable({
  providedIn: 'root'
})
export class SharedCustomService extends BaseDfApiService{

  constructor(protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration) {

    super('shared', httpClient, basePath, configuration);
        /*
    if (basePath) {
        this.basePath = basePath;
    }
    if (configuration) {
        this.configuration = configuration;
        this.basePath = basePath || configuration.basePath || this.basePath;
    }

    if (this.configuration.basePath && this.configuration.basePath.length !== 0) {
        this.basePath = this.configuration.basePath + this.basePath;
    }


    // authentication xxx required
    this.defaultHeaders = this.setAuthenticationInHeaders(this.defaultHeaders);
    //queryParameters = this.setAuthenticationInQueryParameters(queryParameters);
    */
}

/**
     *
     * GET accounting period
     * @param organisation_id Local site
     * @param date Action ID
     */
    public getCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public getCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public getCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public getCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string, observe: any = 'body', reportProgress: boolean = false): Observable<any> {

        if (organisation_id === null || organisation_id === undefined) {
            throw new Error('Required parameter organisation_id was null or undefined when calling actionDelete.');
        }
        if (site_id === null || site_id === undefined) {
            throw new Error('Required parameter brsActionId was null or undefined when calling actionDelete.');
        }

        let queryParameters = new HttpParams({ encoder: new CustomHttpUrlEncodingCodec() });

        if (orm_name !== undefined && orm_name !== null) {
            queryParameters = queryParameters.set('orm_name', <any>orm_name);
        }
        if (counter_id !== undefined && counter_id !== null) {
            queryParameters = queryParameters.set('counter_id', <any>counter_id);
        }

        if (organisation_id !== undefined && organisation_id !== null) {
          queryParameters = queryParameters.set('organisation_id', <any>organisation_id);
      }
      if (site_id !== undefined && site_id !== null) {
          queryParameters = queryParameters.set('site_id', <any>site_id);
      }

        return this.runHttpApiRequest(apiMethod.get ,`counter`, observe, reportProgress, queryParameters);
    }
    public postCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string,counter_value  : number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string,counter_value  : number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string,counter_value  : number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postCounter(orm_name: string, counter_id : string, organisation_id: string, site_id: string,counter_value  : number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {

      if (organisation_id === null || organisation_id === undefined) {
          throw new Error('Required parameter organisation_id was null or undefined when calling actionDelete.');
      }
      if (site_id === null || site_id === undefined) {
          throw new Error('Required parameter brsActionId was null or undefined when calling actionDelete.');
      }

      let queryParameters = new HttpParams({ encoder: new CustomHttpUrlEncodingCodec() });

      if (orm_name !== undefined && orm_name !== null) {
          queryParameters = queryParameters.set('orm_name', <any>orm_name);
      }
      if (counter_id !== undefined && counter_id !== null) {
          queryParameters = queryParameters.set('counter_id', <any>counter_id);
      }

      if (organisation_id !== undefined && organisation_id !== null) {
        queryParameters = queryParameters.set('organisation_id', <any>organisation_id);
    }
    if (site_id !== undefined && site_id !== null) {
        queryParameters = queryParameters.set('site_id', <any>site_id);
    }

    if (counter_value !== undefined && counter_value !== null) {
      queryParameters = queryParameters.set('counter_value', <any>counter_value);
  }

      return this.runHttpApiRequest(apiMethod.post ,`counter`, observe, reportProgress, queryParameters);
  }

    runHttpApiRequest(apiVerb: apiMethod, apiEndpoint: string, observe: any, reportProgress: boolean, queryParameters?: HttpParams, queryBody?: any): Observable<any> {

      let headers = this.defaultHeaders;

      // authentication xxx required
      headers = this.setAuthenticationInHeaders(headers);
      if (queryParameters) {
          queryParameters = this.setAuthenticationInQueryParameters(queryParameters);
      }

      // to determine the Accept header
      headers = this.setHeaderAccepts(['application/json', 'application/xml'], headers);

      // to determine the Content-Type header
      const consumes: string[] = [
      ];

      switch (apiVerb) {
          case apiMethod.get:

              return this.requestGet(this.serviceName,
                  apiEndpoint,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);

          case apiMethod.post:

              return this.requestPost(this.serviceName,
                  apiEndpoint,
                  queryBody,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);

          case apiMethod.patch:

              return this.requestPatch(this.serviceName,
                  apiEndpoint,
                  queryBody,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);

          case apiMethod.delete:

              return this.requestDelete(this.serviceName,
                  apiEndpoint,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);


          default:
              return undefined;
              break;
      }

  }
}
