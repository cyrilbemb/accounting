/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DataDispatchService } from './data-dispatch.service';

describe('Service: DataDispatch', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataDispatchService]
    });
  });

  it('should ...', inject([DataDispatchService], (service: DataDispatchService) => {
    expect(service).toBeTruthy();
  }));
});
