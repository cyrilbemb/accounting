import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataDispatchService {

public static LANG_FRENCH : string = 'FRA';
public static LANG_ENGLISH : string = 'ENG';

selectedLang : string;

constructor() { }


langToISO3() : string{
  let iso3Lang : string  ='';

  switch(this.selectedLang){
    case 'fr':
      iso3Lang =  DataDispatchService.LANG_FRENCH;
      break;
    case 'en':
      iso3Lang = DataDispatchService.LANG_ENGLISH;
      break;
    default:
      break;
  }

  return iso3Lang;
}

}
