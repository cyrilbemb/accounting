/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DataParserService } from './data-parser.service';

describe('Service: DataParser', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataParserService]
    });
  });

  it('should ...', inject([DataParserService], (service: DataParserService) => {
    expect(service).toBeTruthy();
  }));
});
