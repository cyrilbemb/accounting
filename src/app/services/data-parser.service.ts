import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';

import {parse, validate} from 'fast-xml-parser';
@Injectable({
  providedIn: 'root'
})
export class DataParserService {

constructor() { }


stringToObjectList(file : File,fieldSeparator : string,recordSeparator, limit ?: number) : Observable<any[]>{

  return Observable.create(
    (sub: Subscriber<any[]>): void => {
        const reader = new FileReader;
        // if success
        reader.onload = function(event) {
          let result = reader.result;
          let recordList : any[] = result.toString().split(recordSeparator,limit);

          recordList.forEach(record => {
            //console.log(recordList);
            let tstrecord = record.split(fieldSeparator);
            recordList[recordList.indexOf(record)] = tstrecord;
          });

          sub.next(recordList);
        };
        // if failed
        reader.onerror = (ev: ProgressEvent): void => {
            sub.error(ev);
        };
        reader.readAsText(file);
    }
);

}

jsonToObjectList(file : File) : Observable<any[]>{

  return Observable.create(
    (sub: Subscriber<any[]>): void => {
        const reader = new FileReader;
        // if success
        reader.onload = function(event) {
          let result = reader.result;
          console.log('tst',JSON.parse(result.toString()));
          sub.next(JSON.parse(result.toString()));
        };
        // if failed
        reader.onerror = (ev: ProgressEvent): void => {

          console.log(ev);
            sub.error(ev);
        };
        reader.readAsText(file);
    }
);

}

xmlToObjectList(file : File) : Observable<any[]>{

  return Observable.create(
    (sub: Subscriber<any[]>): void => {
        const reader = new FileReader;
        // if success
        reader.onload = function(event) {
          let result = reader.result;
          console.log('tst',result);
          let jsonObj;
          if( validate(result.toString()) === true) { //optional (it'll return an object in case it's not valid)
            jsonObj = parse(result.toString());
          }
          console.log('JSON',jsonObj);
          sub.next(Object.keys(jsonObj[Object.keys(jsonObj)[0]]));
        };
        // if failed
        reader.onerror = (ev: ProgressEvent): void => {

          console.log(ev);
            sub.error(ev);
        };
        reader.readAsText(file);
    }
);

}

}


