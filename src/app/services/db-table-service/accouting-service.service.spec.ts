/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AccoutingServiceService } from './accouting-service.service';

describe('Service: AccoutingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccoutingServiceService]
    });
  });

  it('should ...', inject([AccoutingServiceService], (service: AccoutingServiceService) => {
    expect(service).toBeTruthy();
  }));
});
