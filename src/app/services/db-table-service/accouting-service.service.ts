import { Injectable, Optional, Inject } from '@angular/core';
import { BaseDfApiService, BASE_PATH, Configuration, CustomHttpUrlEncodingCodec } from 'eaf-lib';
import { HttpClient, HttpResponse, HttpEvent, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';


enum apiMethod {
  get,
  post,
  put,
  patch,
  delete
}
@Injectable({
  providedIn: 'root'
})
export class AccoutingServiceService extends BaseDfApiService{

  constructor(protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration) {

    super('accounting', httpClient, basePath, configuration);
        /*
    if (basePath) {
        this.basePath = basePath;
    }
    if (configuration) {
        this.configuration = configuration;
        this.basePath = basePath || configuration.basePath || this.basePath;
    }

    if (this.configuration.basePath && this.configuration.basePath.length !== 0) {
        this.basePath = this.configuration.basePath + this.basePath;
    }


    // authentication xxx required
    this.defaultHeaders = this.setAuthenticationInHeaders(this.defaultHeaders);
    //queryParameters = this.setAuthenticationInQueryParameters(queryParameters);
    */
}

/**
     *
     * GET accounting period
     * @param organisation_id Local site
     * @param date Action ID
     */
    public getPeriod(organisation_id: string, date: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public getPeriod(organisation_id: string, date: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public getPeriod(organisation_id: string, date: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public getPeriod(organisation_id: string, date: string, observe: any = 'body', reportProgress: boolean = false): Observable<any> {

        if (organisation_id === null || organisation_id === undefined) {
            throw new Error('Required parameter organisation_id was null or undefined when calling actionDelete.');
        }
        if (date === null || date === undefined) {
            throw new Error('Required parameter brsActionId was null or undefined when calling actionDelete.');
        }

        let queryParameters = new HttpParams({ encoder: new CustomHttpUrlEncodingCodec() });

        if (organisation_id !== undefined && organisation_id !== null) {
            queryParameters = queryParameters.set('organisation_id', <any>organisation_id);
        }
        if (date !== undefined && date !== null) {
            queryParameters = queryParameters.set('date', <any>date);
        }

        return this.runHttpApiRequest(apiMethod.get ,`period`, observe, reportProgress, queryParameters);
    }

    runHttpApiRequest(apiVerb: apiMethod, apiEndpoint: string, observe: any, reportProgress: boolean, queryParameters?: HttpParams, queryBody?: any): Observable<any> {

      let headers = this.defaultHeaders;

      // authentication xxx required
      headers = this.setAuthenticationInHeaders(headers);
      if (queryParameters) {
          queryParameters = this.setAuthenticationInQueryParameters(queryParameters);
      }

      // to determine the Accept header
      headers = this.setHeaderAccepts(['application/json', 'application/xml'], headers);

      // to determine the Content-Type header
      const consumes: string[] = [
      ];

      switch (apiVerb) {
          case apiMethod.get:

              return this.requestGet(this.serviceName,
                  apiEndpoint,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);

          case apiMethod.post:

              return this.requestPost(this.serviceName,
                  apiEndpoint,
                  queryBody,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);

          case apiMethod.patch:

              return this.requestPatch(this.serviceName,
                  apiEndpoint,
                  queryBody,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);

          case apiMethod.delete:

              return this.requestDelete(this.serviceName,
                  apiEndpoint,
                  queryParameters,
                  headers,
                  observe,
                  reportProgress);


          default:
              return undefined;
              break;
      }

  }
}
