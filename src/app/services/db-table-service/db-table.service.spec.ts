/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DbTableService } from './db-table.service';

describe('Service: DbTable', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DbTableService]
    });
  });

  it('should ...', inject([DbTableService], (service: DbTableService) => {
    expect(service).toBeTruthy();
  }));
});
