import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DbTableService {
  dialogData: any;

constructor() { }


getDialogData() {
  return this.dialogData;
}

// DEMO ONLY, you can find working methods below
addItem (item: any): void {
  this.dialogData = item;
}

updateItem (item: any): void {
  console.log("DIALOG DATA", item);
  this.dialogData = item;
}

deleteItem (item: any): void {
  //console.log(id);
}

}




