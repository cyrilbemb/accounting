/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OrmAccountingService } from './orm-accounting.service';

describe('Service: OrmAccounting', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrmAccountingService]
    });
  });

  it('should ...', inject([OrmAccountingService], (service: OrmAccountingService) => {
    expect(service).toBeTruthy();
  }));
});
