import { Injectable, Optional, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Configuration, BASE_PATH, RecordsRequest, DatabaseDfApiService } from 'eaf-lib';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrmAccountingService extends DatabaseDfApiService{


constructor(protected httpClient: HttpClient,
  @Optional() configuration: Configuration,
  @Optional() @Inject(BASE_PATH) basePath: string
) {
  super('orm_accounting', httpClient, basePath, configuration);
}



public getSchemaDetails(tableName: string, refresh?: boolean, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
     let schema = this.describeTable(tableName, refresh, observe, reportProgress);
return schema;
}

public getRecords(tableName: string, fields?: Array<string>, related?: Array<string>, filter?: string, limit?: number): Observable<any> {
let records = this.getTableRecords(tableName, fields, related, filter, limit);
return records;
}

public postRecord(tableName : string, body : RecordsRequest<Object>,cont ?: boolean): Observable<any>{

let record = this.createTableRecords(tableName, body,null,null,null,null,null,cont);
return record;

}

public patchRecord(id : string, tableName : string, body : RecordsRequest<Object>): Observable<any>{
let record = this.updateTableRecord(id,tableName, body);
return record;
}

public deleteRecord(id : string, tableName : string): Observable<any>{
let record = this.deleteTableRecord(id,tableName);
return record;
}

public deleteRecords(tableName : string,body?: RecordsRequest<any>, fields?: Array<string>, related?: Array<string>, ids?: Array<number | string>, idField?: Array<string>, idType?: Array<string>, _continue?: boolean, rollback?: boolean, filter?: string): Observable<any>{
  let record = this.deleteTableRecords(tableName,body,fields,related,ids,idField,idType,_continue,rollback,filter);
  return record;


}

public patchRecords(tableName : string, body : RecordsRequest<Object>): Observable<any>{
  let record = this.updateTableRecords(tableName, body);
  return record;
  }

}
