/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OrmAmdirectoryService } from './orm-amdirectory.service';

describe('Service: OrmAmdirectory', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrmAmdirectoryService]
    });
  });

  it('should ...', inject([OrmAmdirectoryService], (service: OrmAmdirectoryService) => {
    expect(service).toBeTruthy();
  }));
});
