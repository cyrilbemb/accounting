import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatExpansionPanelActionRow } from '@angular/material';
import { IAccountingPeriod } from 'src/app/models/accounting-period';
import { OrmAccountingService } from 'src/app/services/db-table-service/orm-accounting.service';
import { IJournal } from 'src/app/models/journal';
import { SimpleDialogComponent } from 'src/app/dialogs/simple-dialog/simple-dialog.component';
import { DataTableInfo } from 'src/app/models/model-table-info';
import { ModelTableObject } from 'src/app/models/model-table-object';
import { TranslateService } from '@ngx-translate/core';
import { DbTableService } from 'src/app/services/db-table-service/db-table.service';
import { OrmAmdirectoryService } from 'src/app/services/db-table-service/orm-amdirectory.service';
import { DataDispatchService } from 'src/app/services/data-dispatch.service';
import { DbTableFormComponent } from 'src/app/dialogs/db-table-form/db-table-form.component';
import { IJournalEntry } from 'src/app/models/journal-entry';
import { Moment } from 'moment';
import * as moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import { SharedCustomService } from 'src/app/services/custom-services/shared-custom.service';
import { IJournalPiece } from 'src/app/models/journal-piece';
import { IDcList } from 'src/app/models/dc-list';

@Component({
  selector: 'app-table-entry',
  templateUrl: './table-entry.component.html',
  styleUrls: ['./table-entry.component.css']
})


export class TableEntryComponent implements OnInit {

  public _current_piece : IJournalPiece;
  public _isNewPiece: boolean;
  public _isSaveAction : number = 0;

  public newDate : Moment;

  public newJournalEntries : IJournalEntry[] = [];
  public updateJournalEntries : IJournalEntry[] = [];
  public deleteJournalEntries : IJournalEntry[] = [];

  selectedRowIndex: any;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @Input() set current_piece(piece : IJournalPiece){
    this._current_piece = piece;
      this.retrieveEntries();
  }

  get current_piece() : IJournalPiece{
    return this._current_piece;
  }

  @Input() set isNewPiece(bool : boolean){
    this._isNewPiece = bool;
    if(this._isNewPiece){
      this.newJournalEntries = [];
      this.refreshTable(this.newJournalEntries);
    }else{
      this.retrieveEntries();
    }
  }

  get isNewPiece() : boolean{
    return this._isNewPiece;
  }

  @Input() set isSaveAction(action : number){
    if(action != undefined){

      console.log("TST SAVE",action);

      let newEntriesResource : Array<any> = [];

      if(this.newJournalEntries.length > 0 && this.debitCreditSum(this.newJournalEntries) == 0) {
        this.sharedService.getCounter('orm_accounting','journal.piece','EAA','EAF').subscribe(result => {


          this.current_piece.journal_piece_id = result.resource;
          this.newJournalEntries.forEach(journalEntry => {
            journalEntry.journal_entry_item = result.resource;
            journalEntry.journal_piece_uid =  this.current_piece.uuid;

            newEntriesResource.push(journalEntry);
          });

          this.ormAccounting.postRecord('journal_piece',{resource : [this.current_piece]}).subscribe(result => {
            this.newJournalEntries = [];
            this.ormAccounting.postRecord('journal_entry',{resource : newEntriesResource}).subscribe(result => {
              this.newJournalEntries = [];
            });
          });




        });
      }else if(this.newJournalEntries.length == 0 && this._isNewPiece){
        this.displayErrorDialog(this.translate.instant('error_messages.error_prompt_title'),this.translate.instant('error_messages.error_missing_entries'));
      }else if(this.debitCreditSum(this.newJournalEntries) != 0 && this._isNewPiece){
        this.displayErrorDialog(this.translate.instant('error_messages.error_prompt_title'),this.translate.instant('error_messages.error_dc_zero'));
      }


    }
  }

  displayErrorDialog(title: string, message: string) {
    const formDialog = this.dialog.open(SimpleDialogComponent, {
      minWidth: '300px',
      maxHeight: '100vh',
      data: {
        message:  message,
        title: title
      }
    });
  }

  debitCreditSum(list : IJournalEntry[]) : number{
    let sum : number = 0;
    list.forEach(entry  => {
      if(entry.dc == IDcList.DEBIT_TYPE_VALUE){
        sum = sum - entry.amount;
      }else{
        sum += entry.amount;
      }
    });
    console.log(sum);

    return sum;
  }

  get isSaveAction() : number{
    return this._isSaveAction;
  }

  displayedColumns: string[] = ['journal_entry_line','journal_entry_item','journal_entry_comment','account_id','debit','credit','journal_entry_due_date', 'action'];
  dataSourceRow: MatTableDataSource<Object>;

  tableInfo: DataTableInfo;

  constructor(
    public sharedService : SharedCustomService,
    public translate: TranslateService,
    public dataService: DbTableService,
    private ormAmdirectoryService: OrmAmdirectoryService,
    private ormAccounting: OrmAccountingService,
    public dataDispatch : DataDispatchService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.tableInfo = ModelTableObject.getTableModel("journal_entry",this.ormAmdirectoryService, this.ormAccounting,this.dataDispatch, this.translate);

  }

  tableRowClick(item) {

    if (item) {
      this.selectedRowIndex = item.uuid;
    } else {
      this.selectedRowIndex = '';
    }

  }

  handleRowAdd() {

    let resource: Array<any> = [];

      const uldFormDialog = this.dialog.open(DbTableFormComponent, {
        minWidth: '600px',
        maxHeight: '90vh',
        data: {
         tableName: "journal_entry",
         tableRecord: this.getNewItem(),
         translationClassName: "journal_entry"
        }
      });

      uldFormDialog.afterClosed().subscribe(result => {
        if (result === 1) {
          const editedItem = this.dataService.getDialogData();

          console.log(editedItem);
          if(editedItem.dc == IDcList.DEBIT_TYPE_VALUE){
            editedItem.credit = 0;
            editedItem.debit = editedItem.amount;
          }else{
            editedItem.debit = 0;
            editedItem.credit = editedItem.amount;
          }
          if(!this._isNewPiece){

            // When using an edit things are little different, firstly we find record inside DataService by id
            const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === editedItem[this.tableInfo.id_field]);
            // Then you update that record using data from dialogData (values you enetered)

            resource.push(editedItem);

            this.ormAccounting.postRecord("journal_entry", {resource : resource}).subscribe(result => {
              let response = result as Map<String, Array<Map<any, any>>>;

              if (response['resource'][0] != undefined) {
                this.ormAccounting.getRecords("journal_entry", ['*'], null, "(("+this.tableInfo.id_field+"='"+editedItem[this.tableInfo.id_field]+"'))")

                .subscribe(
                  (resultGet) => {
                    console.log("POST RESPONSE",resultGet,editedItem);
                    editedItem[this.tableInfo.id_field] = resultGet['resource'][0][this.tableInfo.id_field];
                    this.tableInfo.additional_displayed_fields.forEach(field => {
                      editedItem[field] = resultGet['resource'][0][field];
                    });

                    this.dataSourceRow.data.push(editedItem);
                    this.refreshTable(this.dataSourceRow.data);
                  });


              } else {
                //this.displayErrorDialog(this.translate.instant('error_prompt_title'), this.translate.instant('error_messages.error_request_failed'));
              }
            });
          }else{

            this.newJournalEntries.push(editedItem);
            this.dataSourceRow.data.push(editedItem);
            this.refreshTable(this.dataSourceRow.data);
          }
          // And lastly refresh table
          // this.refreshTable();

        }
      });

    }

  handleRowEdit(item) {
    let resource: Array<any> = [];
    let ids: Array<any> = [];

    const formItem = Object.assign({}, item);

    const formDialog = this.dialog.open(DbTableFormComponent, {
      minWidth: '600px',
      maxHeight: '90vh',
      data: {
       tableName: "journal_entry",
       tableRecord: formItem,
       organisation_id:  'EAA',
       translationClassName: "journal_entry"
      }
    });

    formDialog.afterClosed().subscribe(result => {
      if (result === 1) {
        console.log('DIALOG OUTPUT',this.dataService.getDialogData());
        if(!this._isNewPiece){
          const editedItem = this.dataService.getDialogData();
          if(editedItem.dc == IDcList.DEBIT_TYPE_VALUE){
            editedItem.credit = 0;
            editedItem.debit = editedItem.amount;
          }else{
            editedItem.debit = 0;
            editedItem.credit = editedItem.amount;
          }
          // When using an edit things are little different, firstly we find record inside DataService by id
          const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
          // Then you update that record using data from dialogData (values you enetered)

          resource.push(editedItem);
          ids.push(editedItem[this.tableInfo.id_field]);

          this.ormAccounting.patchRecords("journal_entry", {resource : resource}).subscribe(result => {
            console.log('PATCH RESPONSE',result);
            let response = result as Map<String, Array<Map<any, any>>>;

            if (result['resource'][0] != undefined/*[this.tableInfo.id_field] === editedItem[this.tableInfo.id_field]*/) {
              this.ormAccounting.getRecords("journal_entry", ['*'], null, "(("+this.tableInfo.id_field+"='"+editedItem[this.tableInfo.id_field]+"'))")

                .subscribe(
                  (resultGet) => {
                    console.log("PATCH RESPONSE",resultGet,editedItem);
                    editedItem[this.tableInfo.id_field] = resultGet['resource'][0][this.tableInfo.id_field];
                    this.tableInfo.additional_displayed_fields.forEach(field => {
                      editedItem[field] = resultGet['resource'][0][field];
                    });
                    this.dataSourceRow.data[foundIndex] = editedItem;
                    this.refreshTable(this.dataSourceRow.data);
                  });


            } else {
              //this.displayErrorDialog(this.translate.instant('error_prompt_title'), this.translate.instant('error_messages.error_request_failed'));
            }
          });
        }else{
          //Check If records exists in the created Journals
          const foundIndexNew = this.newJournalEntries.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
          if(foundIndexNew != -1){
            this.newJournalEntries[foundIndexNew] = this.dataService.getDialogData();
          }
          const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
          this.dataSourceRow.data[foundIndex] = this.dataService.getDialogData();
          this.refreshTable(this.dataSourceRow.data);
        }

      }
    });

  }

  handleRowDel(item) {

    const formItem = Object.assign({}, item);

    const formDialog = this.dialog.open(SimpleDialogComponent, {
    minWidth: '300px',
    maxHeight: '100vh',
    data: {
      message:  this.translate.instant('prompt_delete_admin'),
      title: item[this.tableInfo.main_displayed_field]
    }
  });

  formDialog.afterClosed().subscribe(result => {
    if (result === 1) {
      if(!this._isNewPiece){
        const editedItem = this.dataService.getDialogData();


        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
        // Then you update that record using data from dialogData (values you enetered)
        this.ormAccounting.deleteRecords("journal_entry", null, ['*'], [], [], [], [], null, null, "(("+this.tableInfo.id_field+"='"+item[this.tableInfo.id_field]+"'))").subscribe(result => {
          let response = result as Map<String, Array<Map<any, any>>>;

          if (result['resource'][0][this.tableInfo.id_field] != undefined && result['resource'][0][this.tableInfo.id_field]  != null) {
            this.dataSourceRow.data.splice(foundIndex, 1);
            this.refreshTable(this.dataSourceRow.data);
          } else {
            //this.displayErrorDialog(this.translate.instant('error_prompt_title'), this.translate.instant('error_messages.error_request_failed'));
          }
        });
      }else{
        const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
        //Check If records exists in the created Journals
        const foundIndexNew = this.newJournalEntries.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
        if(foundIndexNew != -1){
          this.newJournalEntries.splice(foundIndexNew,1);
        }
        this.dataSourceRow.data.splice(foundIndex, 1);
        this.refreshTable(this.dataSourceRow.data);

      }

    }
  });
  }

  private refreshTable(updatedData) {
    this.dataSourceRow.data = [...updatedData];
  }

  buildFilterRequest() : string{
    let filters : string = '(';
    if(this._current_piece != undefined){
      filters = filters+"(journal_piece_uid="+this._current_piece.uuid+")";

    }

    filters = filters+')';
    return filters;
  }

  retrieveEntries(){
    if(!this._isNewPiece){
      this.ormAccounting.getRecords('journal_entry',["*"],null,this.buildFilterRequest()).subscribe(result =>{
        this.dataSourceRow = new MatTableDataSource<any>();
        this.dataSourceRow.paginator = this.paginator;

        let resultItems : any[] = result.resource;

          resultItems.sort((a,b) =>
            a.journal_entry_line - b.journal_entry_line
          );

        this.dataSourceRow.data = result.resource as any[];
      });
    }else{
      this.dataSourceRow.data = this.newJournalEntries;
    }
  }

  dateToString(date  : any) : string{
    let momNew : Moment = moment(date);

    return momNew.format(this.translate.instant('date_format'));
  }


  getNewItem(): Object {

    this.newDate = moment(new Date())
       const newJournalEntry: IJournalEntry = {
         uuid: uuidv4(),
         journal_piece_uid : this._current_piece.uuid,
         journal_entry_item : this._current_piece.journal_piece_id,
         duedate : this.newDate.toDate(),
         journal_entry_line : this.dataSourceRow.data.length+1
       };

    return newJournalEntry;
   }

}
