import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { MatSelect, MatDialog, MatSnackBar } from '@angular/material';
import { OrmAccountingService } from 'src/app/services/db-table-service/orm-accounting.service';
import { IJournal } from 'src/app/models/journal';
import { DataDispatchService } from 'src/app/services/data-dispatch.service';
import { IJournalType } from 'src/app/models/journal-type';
import { AccoutingServiceService } from 'src/app/services/db-table-service/accouting-service.service';
import { Moment } from 'moment';
import * as moment from 'moment';
import { IAccountingPeriod } from 'src/app/models/accounting-period';
import { SharedCustomService } from 'src/app/services/custom-services/shared-custom.service';
import { SimpleDialogComponent } from 'src/app/dialogs/simple-dialog/simple-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { v4 as uuidv4 } from 'uuid';
import { IJournalPiece } from 'src/app/models/journal-piece';
import { IJournalEntry } from 'src/app/models/journal-entry';
import { DbTableFormComponent } from 'src/app/dialogs/db-table-form/db-table-form.component';
import { DbTableService } from 'src/app/services/db-table-service/db-table.service';

@Component({
  selector: 'app-table-journal-entry',
  templateUrl: './table-journal-entry.component.html',
  styleUrls: ['./table-journal-entry.component.css']
})
export class TableJournalEntryComponent implements OnInit {


  /** control for the selected bank */
  public bankCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public bankFilterCtrl: FormControl = new FormControl();

  public pieces_available : IJournalPiece[] ;

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;


  /** control for the selected bank for option groups */
  public journalGroupControl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword for option groups */
  public journalGroupsFilterCtrl: FormControl = new FormControl();

  /** list of bank groups filtered by search keyword for option groups */
  public filteredJournalsGroups: ReplaySubject<JournalGroup[]> = new ReplaySubject<JournalGroup[]>(1);

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();




  public journalsList : IJournal[];
  public journalsGrouped : JournalGroup[] = [];

  public selectedJournal : IJournal;
  public selectedDate : Moment;
  public selectedPiece : IJournalPiece;

  public current_period : IAccountingPeriod;
  public current_journal : IJournal;
  public current_number : number;
  public saveAction : number=0;

  public isNewPiece : boolean = false;

  public table_journal_entry : string = 'journal_entry';

  constructor(public snackBar: MatSnackBar,
    public dataService: DbTableService,public translate: TranslateService,public dialog: MatDialog,public sharedCustomService : SharedCustomService,public accountingService : AccoutingServiceService,public ormAccounting : OrmAccountingService,public dataDispatch : DataDispatchService) { }

  ngOnInit() {
    this.selectedDate = moment(new Date());
    this.dateChanged();

    // load the initial bank list
   // this.filteredBankGroups.next(this.copyBankGroups(this.bankGroups));

    // listen for search field value changes
    this.journalGroupsFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBankGroups();
      });
  }

  ngAfterViewInit() {
    //this.setInitialValue();
  }


  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected filterBankGroups() {
  /*  if (!this.bankGroups) {
      return;
    }*/
    // get the search keyword
   /* let search = this.bankGroupsFilterCtrl.value;
    const bankGroupsCopy = this.copyBankGroups(this.bankGroups);
    if (!search) {
      this.filteredBankGroups.next(bankGroupsCopy);
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBankGroups.next(
      bankGroupsCopy.filter(bankGroup => {
        const showBankGroup = bankGroup.name.toLowerCase().indexOf(search) > -1;
        if (!showBankGroup) {
          bankGroup.banks = bankGroup.banks.filter(bank => bank.name.toLowerCase().indexOf(search) > -1);
        }
        return bankGroup.banks.length > 0;
      })
    );*/
  }

  newJournalPiece(){

    this.selectedPiece = undefined;
    this.current_number = undefined;
    this.isNewPiece = true;
    const uldFormDialog = this.dialog.open(DbTableFormComponent, {
      minWidth: '600px',
      maxHeight: '90vh',
      data: {
       tableName: "journal_piece",
       tableRecord: this.getNewItem(),
       organisation_id: 'EAA',
       translationClassName: "journal_piece"
      }
    });


    uldFormDialog.afterClosed().subscribe(result => {
      if (result === 1) {
        this.selectedPiece = this.dataService.getDialogData();


      }else{
        this.isNewPiece = false;
      }
    });
  }

  saveAct(){
    if(this.saveAction == 0 ){
      this.saveAction = 1;
    }else{
      this.saveAction = 0;
    }
  }

  deletedJournalPiece(){

    const formDialog = this.dialog.open(SimpleDialogComponent, {
    minWidth: '300px',
    maxHeight: '100vh',
    data: {
      message:  this.translate.instant('prompt_delete_admin'),
      title: this.selectedPiece.journal_piece_id
    }
  });

  formDialog.afterClosed().subscribe(result => {
    if (result === 1) {

      this.ormAccounting.deleteRecords("journal_entry", null, ['*'], [], [], [], [], null, null, "(journal_piece_uid='"+this.selectedPiece.uuid+"')").subscribe(result =>{
        if(result.resource.length>0){
          this.ormAccounting.deleteRecord(this.selectedPiece.uuid,"journal_piece").subscribe(result =>{
            if(result.uuid != undefined){
              this.snackBar.open(this.translate.instant('delete_succes'), this.translate.instant('ok'), {
                duration: 5000,
                panelClass: ['success-snackbar']
              });
              this.selectedPiece = undefined;
              this.pieces_available.splice(this.pieces_available.indexOf(this.selectedPiece),1);
            }
          });

        }
      });
    }
  });
  }

  closePiece(){
    this.isNewPiece = false;
    this.selectedPiece = undefined;
    this.current_number = undefined;
    this.journalChanged();
  }

  dateChanged(){
    this.current_period = undefined;
    this.current_journal = undefined;
    this.current_number = undefined;
    this.selectedPiece = undefined;
    this.pieces_available = undefined;
    console.log('ES',this.selectedDate.year()+'-'+(this.selectedDate.month()+1)+'-'+this.selectedDate.date());
    this.accountingService.getPeriod('EAA',this.selectedDate.year()+'-'+this.selectedDate.month()+'-'+this.selectedDate.daysInMonth()).subscribe(es => {
      console.log('ES',es);
      if(es.resource.error == undefined && es.resource.status != 'not_found' ){


        this.ormAccounting.getRecords('journal',["*"],null,"(organisation_id='EAA')").subscribe(result =>{
          console.log('RESULT JOURNAL LIST',result);

          if(result.resource.length >0){
            this.journalsList = result.resource;
            this.ormAccounting.getRecords('journal_type',["*"],null,"(lang='"+this.dataDispatch.langToISO3()+"')").subscribe(result =>{
              console.log('RESULT JOURNAL TYPE LIST',result);
              this.current_period = es.resource;
              this.journalsGrouped = [];
              this.journalsList.forEach(journal => {
                 if(this.journalsGrouped.find(x => x.type.journal_type_id === journal.journal_type_id) != undefined){
                  this.journalsGrouped[this.journalsGrouped.indexOf(this.journalsGrouped.find(x => x.type.journal_type_id === journal.journal_type_id))].journals.push(journal);
                 }else{
                   let newGroup : JournalGroup = new JournalGroup();
                   newGroup.type = result.resource.find(x => x.journal_type_id === journal.journal_type_id);
                   newGroup.journals = [];
                   newGroup.journals.push(journal);

                   this.journalsGrouped.push(newGroup);

                 }
              });

              this.filteredJournalsGroups.next(this.journalsGrouped);

              console.log('JOUNRALS GROUPED',this.journalsGrouped,this.filteredJournalsGroups);
            });
          }

        });
      }
    });
  }

  journalChanged(){
    this.current_journal = this.selectedJournal;
    this.ormAccounting.getRecords('journal_piece',["*"],null,"((journal_id='"+this.current_journal.id +"') AND (organisation_id='EAA'))").subscribe(result =>{
      console.log(result);
      this.pieces_available = result.resource;
    });
  }

  pieceChanged(event){
    if(this.selectedPiece != undefined){
      this.current_number = this.selectedPiece.accounting_period_number;
    }else{
      this.current_number = undefined;
    }
  }

  handlePieceEdit(){
    let resource: Array<any> = [];
    let ids: Array<any> = [];

    const formItem = Object.assign({}, this.selectedPiece);

    const formDialog = this.dialog.open(DbTableFormComponent, {
      minWidth: '600px',
      maxHeight: '90vh',
      data: {
       tableName: "journal_piece",
       tableRecord: formItem,
       organisation_id:  'EAA',
       translationClassName: "journal_piece"
      }
    });


    formDialog.afterClosed().subscribe(result => {
      if (result === 1) {
        console.log('DIALOG OUTPUT',this.dataService.getDialogData());

          const editedItem = this.dataService.getDialogData();

          // When using an edit things are little different, firstly we find record inside DataService by id
          let ind = this.pieces_available.indexOf(this.pieces_available.find(x => x.uuid == this.selectedPiece.uuid))
          this.pieces_available[ind] = editedItem;

          this.selectedPiece = editedItem;
          // Then you update that record using data from dialogData (values you enetered)

          resource.push(editedItem);


          this.ormAccounting.patchRecords("journal_piece", {resource : resource}).subscribe(result => {
            console.log('PATCH RESPONSE',result);
            let response = result as Map<String, Array<Map<any, any>>>;

            if (result['resource'][0] != undefined/*[this.tableInfo.id_field] === editedItem[this.tableInfo.id_field]*/) {



            } else {
              //this.displayErrorDialog(this.translate.instant('error_prompt_title'), this.translate.instant('error_messages.error_request_failed'));
            }
          });
        }


    });

  }

  getNewItem(): Object {

    let newDate = moment(new Date())
       const newJournalPiece: IJournalPiece = {
         uuid: uuidv4(),
         journal_id : this.current_journal.id,
         accounting_period_id : this.current_period.accounting_period_id,
         organisation_id : 'EAA',
         site_id : 'EAF',
         journal_entry_date : new Date(),
         journal_entry_due_date : new Date(),
         journal_entry_value_date : new Date(),
         duedate : new Date()


        };

    return newJournalPiece;
   }

}

export class JournalGroup {
  type: IJournalType;
  journals: IJournal[];
}
