import { Component, OnInit, Input } from '@angular/core';
import { TableDetailsComponent } from 'src/app/table-details/table-details.component';
import { ModelSettings } from 'src/app/models/model-settings';
import { IJournalType, JournalTypeTable } from 'src/app/models/journal-type';
import { FieldSchema, TableSchema, Site, ResourceResponse } from 'eaf-lib';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { DataTableInfo } from 'src/app/models/model-table-info';
import { IAccountingParameters } from 'src/app/models/accounting-parameters';
import { Subscription, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { DbTableService } from 'src/app/services/db-table-service/db-table.service';
import { OrmAccountingService } from 'src/app/services/db-table-service/orm-accounting.service';
import { ModelTableObject } from 'src/app/models/model-table-object';
import { map, catchError } from 'rxjs/operators';
import { DbTableFormComponent } from 'src/app/dialogs/db-table-form/db-table-form.component';
import { SimpleDialogComponent } from 'src/app/dialogs/simple-dialog/simple-dialog.component';
import { DataDispatchService } from 'src/app/services/data-dispatch.service';
import { OrmAmdirectoryService } from 'src/app/services/db-table-service/orm-amdirectory.service';

@Component({
  selector: 'app-table-journal-type',
  templateUrl: '../../table-details/table-details.component.html',
  styleUrls: ['../../table-details/table-details.component.css']
})
export class TableJournalTypeComponent implements OnInit {

  @Input() tableName: string;

  private modelSettings = ModelSettings;

  fieldList:  Array<FieldSchema>;
  schema: TableSchema;
  dataSourceRow: MatTableDataSource<Object>;
  displayedColumns: string[] ;
  tableInfo: DataTableInfo;
  siteList: Site[];
  displayedColumnsMap: Map<String, string[]> = new Map<String, string[]>();

  dataTest: Map<String, Array<Object>>;

  paramsArray: Array<IAccountingParameters>;
  public fieldNames: string[] = [];
  public fieldToDisplay: string[] = [];
  selectedRowIndex: any;
  field: string;
  flightSubscription: Subscription;
  filterRequest: string;

  tableRecordSave: any;

  isLoadingData = false;

  constructor(
    public translate: TranslateService,
    public dataService: DbTableService,
    private ormAccountingService: OrmAccountingService,
    private ormAmdirectoryService: OrmAmdirectoryService,
    public dataDispatch : DataDispatchService,
    public dialog: MatDialog) {

  }

  ngOnInit() {

    this.tableInfo = ModelTableObject.getTableModel(this.tableName, this.ormAmdirectoryService,this.ormAccountingService,this.dataDispatch, this.translate);

    this.loadHeaders();

   /* this.flightFilterService.changedFullFlightFilter.subscribe(() => {

      this.loadHeaders();
    });*/
  }

  private loadHeaders() {

    this.ormAccountingService.getSchemaDetails(this.tableName, false, null, false)
      .subscribe(
      (dataRow) => {
        this.schema = dataRow as TableSchema;
        this.fieldList = this.schema.field;
        this.fieldNames = [];
        this.fieldToDisplay = [];

        for (const column of this.fieldList) {

          this.fieldNames.push(column.name);

         if (this.isColumnShown(this.tableInfo, column.name)) {
            this.fieldToDisplay.push(column.name);
          }

        }

        this.fieldToDisplay.push('action');

        this.displayedColumnsMap.set(this.tableName, this.fieldToDisplay);

        this.displayedColumns = this.fieldToDisplay;
        this.loadDataRow();

      });

  }

  private loadDataRow() {

    this.isLoadingData = true;

    this.ormAccountingService.getRecords(this.tableName, ['*'], null, this.buildFilterRequest())
      .pipe(
        map((data: ResourceResponse<any>) => {
          // Flip flag to show that loading has finished.
          this.isLoadingData = false;
          return data.resource;
        }),
        catchError(() => {
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          // this.isRateLimitReached = true;
          return of([]);
        })
      )
      .subscribe(
        (dataRow) => {
          this.isLoadingData = false;

          this.dataSourceRow = new MatTableDataSource<any>();
          this.dataSourceRow.data = dataRow as any[];
        });

  }

   buildFilterRequest(): string {

    if ( this.fieldNames.indexOf('organisation_id') !== -1) {
     // this.siteList = this.flightFilterService.allowedSites as Site[];
      // const sitenames = this.siteList.reduce((acc, site) => [...acc, site.site_id], []).join(',');
      // this.filterRequest = `(site_id in ())`;
      this.filterRequest = 'organisation_id=' + 'EAA';


    } else {
      this.filterRequest = '';
    }
    return this.filterRequest;
 }

 getColumnWidth(column) {
   return this.tableInfo.field_width.get(column) > 0 ? this.tableInfo.field_width.get(column) : (100 / this.fieldToDisplay.length);
 }

 getCellValue(row, column) {

  if (row === undefined || column ===  undefined) {
    return '';
  } else {
    if (this.tableInfo.field_access.get(column) === ModelSettings.FIELD_CHECKBOX) {
      return '';
    } else {
      return row[column];
    }
  }

 }

 getCellIcon(row, column) {

  if (this.tableInfo.field_access.get(column) === ModelSettings.FIELD_CHECKBOX) {
    return (row[column] > 0 ? 'check_box' : 'crop_square');
  } else {
    return '';
  }

 }

 tableRowClick(item) {

  if (item) {
    this.selectedRowIndex = item[this.tableInfo.id_field];
  } else {
    this.selectedRowIndex = '';
  }

}

isColumnShown(tableInfo: DataTableInfo, column: string) {
  let isOk: Boolean = false;

  if (tableInfo.field_access.get(column) !== ModelSettings.FIELD_HIDDEN && tableInfo.fields_hidden_in_table.indexOf(column) == -1 ) {
    isOk = true;
  }

  return isOk;
}

handleRowAdd() {

  let resource: Array<any> = [];

    const uldFormDialog = this.dialog.open(DbTableFormComponent, {
      minWidth: '600px',
      maxHeight: '90vh',
      data: {
       tableName: this.tableName,
       tableRecord: this.getNewItem(),
       organisation_id: 'EAA',
       translationClassName: this.tableName
      }
    });

    uldFormDialog.afterClosed().subscribe(result => {
      if (result === 1) {

        const editedItem = this.dataService.getDialogData();

        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === editedItem[this.tableInfo.id_field]);
        // Then you update that record using data from dialogData (values you enetered)

        resource.push(editedItem);

        this.ormAccountingService.postRecord(this.tableName, {resource : resource}).subscribe(result => {
          let response = result as Map<String, Array<Map<any, any>>>;

          if (response['resource'][0] != undefined) {
            this.ormAccountingService.getRecords(this.tableName, ['*'], null, "((organisation_id='EAA') AND ("+this.tableInfo.id_field+"='"+editedItem[this.tableInfo.id_field]+"'))")

            .subscribe(
              (resultGet) => {
                console.log("POST RESPONSE",resultGet,editedItem);
                editedItem[this.tableInfo.id_field] = resultGet['resource'][0][this.tableInfo.id_field];
                this.tableInfo.additional_displayed_fields.forEach(field => {
                  editedItem[field] = resultGet['resource'][0][field];
                });
                this.dataSourceRow.data.push(editedItem);
                this.refreshTable(this.dataSourceRow.data);
              });



          } else {
            this.displayErrorDialog(this.translate.instant('error_prompt_title'), this.translate.instant('error_messages.error_request_failed'));
          }
        });

        // And lastly refresh table
        // this.refreshTable();

      }
    });

  }

handleRowEdit(item) {
  let resource: Array<any> = [];
  let ids: Array<any> = [];

  const formItem = Object.assign({}, item);

  const formDialog = this.dialog.open(DbTableFormComponent, {
    minWidth: '600px',
    maxHeight: '90vh',
    data: {
     tableName: this.tableName,
     tableRecord: formItem,
     organisation_id:  'EAA',
     translationClassName: this.tableName
    }
  });

  formDialog.afterClosed().subscribe(result => {
    if (result === 1) {

      const editedItem = this.dataService.getDialogData();
      // When using an edit things are little different, firstly we find record inside DataService by id
      const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
      // Then you update that record using data from dialogData (values you enetered)

      resource.push(editedItem);
      ids.push(editedItem[this.tableInfo.id_field]);

      this.ormAccountingService.patchRecords(this.tableName, {resource : resource}).subscribe(result => {
        console.log('PATCH RESPONSE',result);
        let response = result as Map<String, Array<Map<any, any>>>;

        if (result['resource'][0] != undefined/*[this.tableInfo.id_field] === editedItem[this.tableInfo.id_field]*/) {
          this.ormAccountingService.getRecords(this.tableName, ['*'], null, "((organisation_id='EAA') AND ("+this.tableInfo.id_field+"='"+editedItem[this.tableInfo.id_field]+"'))")

            .subscribe(
              (resultGet) => {
                console.log("PATCH RESPONSE",resultGet,editedItem);
                editedItem[this.tableInfo.id_field] = resultGet['resource'][0][this.tableInfo.id_field];
                this.tableInfo.additional_displayed_fields.forEach(field => {
                  editedItem[field] = resultGet['resource'][0][field];
                });
                this.dataSourceRow.data[foundIndex] = editedItem;
                this.refreshTable(this.dataSourceRow.data);
              });


        } else {
          this.displayErrorDialog(this.translate.instant('error_prompt_title'), this.translate.instant('error_messages.error_request_failed'));
        }
      });


    }
  });

}

handleRowDel(item) {

  console.log('DELETED ITEM', item);
  const formItem = Object.assign({}, item);

    const formDialog = this.dialog.open(SimpleDialogComponent, {
    minWidth: '300px',
    maxHeight: '100vh',
    data: {
      message:  this.translate.instant('prompt_delete_admin'),
      title: item[this.tableInfo.main_displayed_field]
    }
  });

  formDialog.afterClosed().subscribe(result => {
    if (result === 1) {
      const editedItem = this.dataService.getDialogData();


      // When using an edit things are little different, firstly we find record inside DataService by id
      const foundIndex = this.dataSourceRow.data.findIndex(x => x[this.tableInfo.id_field] === item[this.tableInfo.id_field]);
      // Then you update that record using data from dialogData (values you enetered)
      this.ormAccountingService.deleteRecords(this.tableName, null, ['*'], [], [], [], [], null, null, "((organisation_id='EAA') AND ("+this.tableInfo.id_field+"='"+item[this.tableInfo.id_field]+"'))").subscribe(result => {
        let response = result as Map<String, Array<Map<any, any>>>;

        if (result['resource'][0][this.tableInfo.id_field] != undefined && result['resource'][0][this.tableInfo.id_field]  != null) {
          this.dataSourceRow.data.splice(foundIndex, 1);
          this.refreshTable(this.dataSourceRow.data);
        } else {
          this.displayErrorDialog(this.translate.instant('error_prompt_title'), this.translate.instant('error_messages.error_request_failed'));
        }
      });

    }
  });

}

private refreshTable(updatedData) {
  this.dataSourceRow.data = [...updatedData];
}

displayErrorDialog(title: string, message: string) {
  const formDialog = this.dialog.open(SimpleDialogComponent, {
    minWidth: '300px',
    maxHeight: '100vh',
    data: {
      message:  message,
      title: title
    }
  });
}

getNewItem(): Object {


  const newJournalType: IJournalType = {
    id:'',
    lang : 'ENG'};
  return newJournalType;
}

addMonth(number : number,from : Date) : Date{
  let date= new Date(from);

  console.log('from',from);
  date.setUTCHours(date.getUTCHours() + 1*12 +1);
  date.setUTCMonth(date.getUTCMonth() + number +1);
  return date;
}
addDay(number : number,from : Date) : Date{
  let date= new Date(from);

  console.log('from',from);
  date.setUTCHours(date.getUTCHours() + number*24 +1);
  return date;
}

getHighest(field : string, items : any[]) : number{

  let maxItemIndex : number = 0;
  items.forEach(element => {
    if (element[field] > items[maxItemIndex][field]){
      maxItemIndex = items.indexOf(element);
    }
  });

  return maxItemIndex;
}


getCurrentLang() : string{

  this.translate.onLangChange.subscribe(yrdy=> {
    console.log('lang change');
    return '';
  });
  return null;
}

}
