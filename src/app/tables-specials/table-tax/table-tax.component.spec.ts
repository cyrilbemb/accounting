/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TableTaxComponent } from './table-tax.component';

describe('TableTaxComponent', () => {
  let component: TableTaxComponent;
  let fixture: ComponentFixture<TableTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
