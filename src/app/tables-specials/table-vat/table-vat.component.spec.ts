/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TableVatComponent } from './table-vat.component';

describe('TableVatComponent', () => {
  let component: TableVatComponent;
  let fixture: ComponentFixture<TableVatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableVatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableVatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
